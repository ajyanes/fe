package com.btw.fe.venta.controller;

import com.btw.fe.venta.domain.mappers.Factura;
import com.btw.fe.venta.service.SaleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/ventas")
public class SaleController {

  @Autowired
  private SaleService saleService;

  /*@GetMapping("/getdata")
  public ResponseEntity<Factura> getFactura() {
    return new ResponseEntity<>(saleService.data(), HttpStatus.ACCEPTED);
  }*/

  @PostMapping("/loadFile")
  @ResponseBody
  ResponseEntity<Factura> loadFile(@RequestParam("file") MultipartFile file) {
    return new ResponseEntity<>(saleService.loadFile(file), HttpStatus.ACCEPTED);
  }

}
