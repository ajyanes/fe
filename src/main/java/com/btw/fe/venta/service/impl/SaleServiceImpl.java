package com.btw.fe.venta.service.impl;

import com.btw.fe.venta.domain.Address;
import com.btw.fe.venta.domain.Contact;
import com.btw.fe.venta.domain.Detail;
import com.btw.fe.venta.domain.FacturaVenta;
import com.btw.fe.venta.domain.Item;
import com.btw.fe.venta.domain.Payment;
import com.btw.fe.venta.domain.Psl;
import com.btw.fe.venta.domain.Resolucion;
import com.btw.fe.venta.domain.Totals;
import com.btw.fe.venta.domain.cliente.Customer;
import com.btw.fe.venta.domain.facturador.DireccionFiscal;
import com.btw.fe.venta.domain.facturador.Facturador;
import com.btw.fe.venta.domain.mappers.AtributosPsl;
import com.btw.fe.venta.domain.mappers.Cliente;
import com.btw.fe.venta.domain.mappers.CodigoItem;
import com.btw.fe.venta.domain.mappers.Contacto;
import com.btw.fe.venta.domain.mappers.Detalle;
import com.btw.fe.venta.domain.mappers.DirFiscal;
import com.btw.fe.venta.domain.mappers.Direccion;
import com.btw.fe.venta.domain.mappers.Factura;
import com.btw.fe.venta.domain.mappers.Facturadorelectronico;
import com.btw.fe.venta.domain.mappers.FormaPago;
import com.btw.fe.venta.domain.mappers.ResolucionDian;
import com.btw.fe.venta.domain.mappers.Totales;
import com.btw.fe.venta.repository.AtributosPslRepository;
import com.btw.fe.venta.repository.ClienteRepository;
import com.btw.fe.venta.repository.CodigoItemRepository;
import com.btw.fe.venta.repository.ContactoRepository;
import com.btw.fe.venta.repository.DetalleRepository;
import com.btw.fe.venta.repository.DirFiscalRepository;
import com.btw.fe.venta.repository.DireccionRepository;
import com.btw.fe.venta.repository.FacturaRepository;
import com.btw.fe.venta.repository.FacturadorelectronicoRepository;
import com.btw.fe.venta.repository.FormaPagoRepository;
import com.btw.fe.venta.repository.ResolucionDianRepository;
import com.btw.fe.venta.repository.TotalesRepository;
import com.btw.fe.venta.service.SaleService;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Slf4j
@Service
public class SaleServiceImpl implements SaleService {

  @Autowired
  private FacturaRepository facturaRepository;
  @Autowired
  private ClienteRepository clienteRepository;
  @Autowired
  private FacturadorelectronicoRepository facturadorelectronicoRepository;
  @Autowired
  private DireccionRepository direccionRepository;
  @Autowired
  private DirFiscalRepository dirFiscalRepository;
  @Autowired
  private ContactoRepository contactoRepository;
  @Autowired
  private FormaPagoRepository formaPagoRepository;
  @Autowired
  private TotalesRepository totalesRepository;
  @Autowired
  private AtributosPslRepository atributosPslRepository;
  @Autowired
  private ResolucionDianRepository resolucionDianRepository;
  @Autowired
  private DetalleRepository detalleRepository;
  @Autowired
  private CodigoItemRepository codigoItemRepository;

  @Override
  public Factura loadFile(MultipartFile file) {
    return getData(file);
  }

  public Factura getData(MultipartFile file) {
    FacturaVenta fv;
    Factura fac = null;
    try {
      JAXBContext context = JAXBContext.newInstance(FacturaVenta.class);
      Unmarshaller unmarshaller = context.createUnmarshaller();
      File finalfile = convertMultiPartToFile(file);
      fv = (FacturaVenta) unmarshaller.unmarshal(finalfile);

      fac = getFactura(fv);

    } catch (JAXBException jaxb) {
      jaxb.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }

    return fac;
  }

  private File convertMultiPartToFile(MultipartFile file) throws IOException {
    File convFile = new File(file.getOriginalFilename());
    FileOutputStream fos = new FileOutputStream(convFile);
    fos.write(file.getBytes());
    fos.close();
    return convFile;
  }

  private Factura getFactura(FacturaVenta fv) {
    Factura fac;
    fac = Factura.builder()
        .ubl(fv.getUbl())
        .versiondian(fv.getVersionDian())
        .tipooperacion(fv.getTipoOperacion())
        .prefijo(fv.getPrefijo())
        .numero(fv.getNumero())
        .pais(fv.getPais())
        .fechaexp(fv.getFechaExp())
        .fechaven(fv.getFechaVen())
        .tipofactura(fv.getTipoFactura())
        .infoadicional(fv.getInfoAdicional().replaceAll("\\s+", " "))
        .moneda(fv.getMoneda())
        .items(fv.getItems())
        .build();

    try {
      facturaRepository.save(fac);
    } catch (Exception ex) {
      ex.getMessage();
    }

    getFacturadorElectronico(fv.getFacturador(), fac.getPrefijo(), fac.getNumero());
    getCustomer(fv.getCustomer(), fac.getPrefijo(), fac.getNumero());
    getFormaPago(fv.getPayment(), fac.getPrefijo(), fac.getNumero());
    getTotales(fv.getTotals(), fac.getPrefijo(), fac.getNumero());
    getPsl(fv.getPsl(), fac.getPrefijo(), fac.getNumero());
    getResolucion(fv.getResolucion(), fac.getPrefijo(), fac.getNumero());
    getDetalle(fv.getDetail(), fac.getPrefijo(), fac.getNumero());

    return fac;
  }

  private void getFacturadorElectronico(Facturador fact, String prefix, String numero) {

    Facturadorelectronico facturadorelectronico = Facturadorelectronico.builder()
        .tipopersona(fact.getTipoPersona())
        .nombrecomercial(fact.getNombreComercial())
        .nombrerazonsocial(fact.getNombreORazonSocial())
        .numeroidentificacion(fact.getNumeroIdentificacion())
        .digitoverificacion(fact.getDigitoVerificacion())
        .tipoid(fact.getTipoId())
        .regimen(fact.getRegimen())
        .responsabilidadfiscal(fact.getResponsabilidadFiscal())
        .obligaciontributaria(fact.getObligacionTributaria())
        .prefijopuntoventa(fact.getPrefijoPuntoVenta())
        .actividadeconomica(fact.getActividadEconomica())
        .prefijo(prefix)
        .numero(numero)
        .build();

    facturadorelectronicoRepository.save(facturadorelectronico);

    getDireccion(fact.getAddress(), facturadorelectronico.getNumeroidentificacion(), prefix,
        numero);
    convertDirFiscalToAddress(fact.getDireccionFiscal(),
        facturadorelectronico.getNumeroidentificacion(), prefix, numero);
    getContacto(fact.getContact(), facturadorelectronico.getNumeroidentificacion(), prefix, numero);

  }

  private void getContacto(Contact con, String numid, String prefix, String numero) {
    Contacto contact = Contacto.builder()
        .nombre(con.getNombre())
        .telefono(con.getTelefono())
        .correoelectronico(con.getCorreoElectronico())
        .numid(numid)
        .prefijo(prefix)
        .numero(numero)
        .build();

    contactoRepository.save(contact);
  }

  private void getDireccion(Address address, String numid, String prefix, String numero) {
    Direccion dir = Direccion.builder()
        .codigociudad(address.getCodigoCiudad())
        .nombreciudad(address.getNombreCiudad())
        .codigopostal(address.getCodigoPostal())
        .codigodepartamento(address.getCodigoDepartamento())
        .nombredepartamento(address.getNombreDepartamento())
        .direccion(address.getDireccion())
        .codigopais(address.getCodigoPais())
        .numid(numid)
        .prefijo(prefix)
        .numero(numero)
        .build();

    direccionRepository.save(dir);
  }

  private void convertDirFiscalToAddress(DireccionFiscal dirFiscal, String numid, String prefix,
      String numero) {
    DirFiscal dirFis = DirFiscal.builder()
        .codigociudad(dirFiscal.getCodigoCiudad())
        .nombreciudad(dirFiscal.getNombreCiudad())
        .codigopostal(dirFiscal.getCodigoPostal())
        .codigodepartamento(dirFiscal.getCodigoDepartamento())
        .nombredepartamento(dirFiscal.getNombreDepartamento())
        .direccion(dirFiscal.getDireccion())
        .codigopais(dirFiscal.getCodigoPais())
        .numid(numid)
        .prefijo(prefix)
        .numero(numero)
        .build();

    dirFiscalRepository.save(dirFis);
  }

  private void getCustomer(Customer customer, String prefix, String numero) {
    Cliente cli = Cliente.builder()
        .tipopersona(customer.getTipoPersona())
        .nombrecomercial(customer.getNombreComercial())
        .nombrerazonsocial(customer.getNombreORazonSocial())
        .numeroidentificacion(customer.getNumeroIdentificacion())
        .digitoverificacion(customer.getDigitoVerificacion())
        .tipoid(customer.getTipoId())
        .regimen(customer.getRegimen())
        .responsabilidadfiscal(customer.getResponsabilidadFiscal())
        .obligaciontributaria(customer.getObligacionTributaria())
        .prefijo(prefix)
        .numero(numero)
        .build();

    clienteRepository.save(cli);

    getDireccion(customer.getAddress(), cli.getNumeroidentificacion(), prefix, numero);
    getContacto(customer.getContact(), cli.getNumeroidentificacion(), prefix, numero);

  }

  private void getFormaPago(Payment payment, String prefix, String numero) {
    FormaPago formaPago = FormaPago.builder()
        .metodopago(payment.getMetodoPago())
        .mediopago(payment.getMedioPago())
        .fechavencimiento(payment.getFechaVencimiento())
        .idpago(payment.getIdPago())
        .prefix(prefix)
        .numero(numero)
        .build();

    formaPagoRepository.save(formaPago);
  }

  private void getTotales(Totals totals, String prefix, String numero) {
    Totales totales = Totales.builder()
        .valortotalbruto(totals.getValorTotalBruto())
        .basegravable(totals.getBaseGravable())
        .valortotalnetoimpuestosincluidos(totals.getValorTotalNetoImpuestosIncluidos())
        .valortotaldescuentos(totals.getValorTotalDescuentos())
        .valortotalcargos(totals.getValorTotalCargos())
        .valortotalanticipos(totals.getValorTotalAnticipos())
        .valorajustedocumento(totals.getValorAjusteDocumento())
        .valortotaldocumento(totals.getValorTotalDocumento())
        .prefix(prefix)
        .numero(numero)
        .build();

    totalesRepository.save(totales);
  }

  private void getPsl(Psl psl, String prefix, String numero) {
    AtributosPsl atributosPsl = AtributosPsl.builder()
        .generarpdf(psl.isGenerarPDF())
        .pdfexterno(psl.isPdfExterno())
        .notificarcliente(psl.isNotificarCliente())
        .correoelectronicocliente(psl.getCorreoElectronicoCliente())
        .prefix(prefix)
        .numero(numero)
        .build();

    atributosPslRepository.save(atributosPsl);
  }

  private void getResolucion(Resolucion resolucion, String prefix, String numero) {
    ResolucionDian resolucionDian = ResolucionDian.builder()
        .resolucion(resolucion.getResolucion())
        .vigenciadesde(resolucion.getVigenciaDesde())
        .vigenciahasta(resolucion.getVigenciaHasta())
        .prefijo(resolucion.getPrefijo())
        .inicioconsecutivo(resolucion.getInicioConsecutivo())
        .finconsecutivo(resolucion.getFinConsecutivo())
        .prefix(prefix)
        .numero(numero)
        .build();

    resolucionDianRepository.save(resolucionDian);
  }

  private void getDetalle(Detail detail, String prefix, String numero) {
    Detalle detalle = Detalle.builder()
        .cantidad(detail.getCantidad())
        .unidadmedida(detail.getUnidadMedida())
        .preciounitario(detail.getPrecioUnitario())
        .preciototalneto(detail.getPrecioTotalNeto())
        .descripcion(detail.getDescripcion())
        .cantidadbaseprecio(detail.getCantidadBasePrecio())
        .unidadmedidabase(detail.getUnidadMedidaBase())
        .prefix(prefix)
        .numero(numero)
        .build();

    detalleRepository.save(detalle);
    getItem(detail.getItem(), prefix, numero);
  }

  private void getItem(Item item, String prefix, String numero) {
    CodigoItem codigoItem = CodigoItem.builder()
        .codigoitem(item.getCodigoItem())
        .estandarcodigoitem(item.getEstandarCodigoItem())
        .prefix(prefix)
        .numero(numero)
        .build();

    codigoItemRepository.save(codigoItem);
  }

}

