package com.btw.fe.venta.service;

import com.btw.fe.venta.domain.mappers.Factura;
import org.springframework.web.multipart.MultipartFile;

public interface SaleService {

  Factura loadFile(MultipartFile file);

}
