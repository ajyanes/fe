package com.btw.fe.venta.domain.mappers;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@IdClass(DetallePk.class)
@Table(name = "ven_detalle")
public class Detalle implements Serializable {

  private int cantidad;
  private String unidadmedida;
  private Long preciounitario;
  private Long preciototalneto;
  private String descripcion;
  private int cantidadbaseprecio;
  private String unidadmedidabase;
  @Id
  private String prefix;
  @Id
  private String numero;

}
