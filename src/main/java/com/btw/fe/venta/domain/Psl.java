package com.btw.fe.venta.domain;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Data;

@Data
@XmlRootElement(name = "atributosPSL")
@XmlAccessorType(XmlAccessType.FIELD)
public class Psl {

  @XmlElement(name = "generarPDF")
  private boolean generarPDF;
  @XmlElement(name = "pdfExterno")
  private boolean pdfExterno;
  @XmlElement(name = "notificarCliente")
  private boolean notificarCliente;
  @XmlElement(name = "correoElectronicoCliente")
  private String correoElectronicoCliente;

}
