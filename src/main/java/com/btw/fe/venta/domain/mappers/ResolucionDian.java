package com.btw.fe.venta.domain.mappers;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@IdClass(ResolucionDianPk.class)
@Table(name = "ven_resoldian")
public class ResolucionDian implements Serializable {

  private String resolucion;
  private Date vigenciadesde;
  private Date vigenciahasta;
  private String prefijo;
  private int inicioconsecutivo;
  private int finconsecutivo;
  @Id
  private String prefix;
  @Id
  private String numero;


}
