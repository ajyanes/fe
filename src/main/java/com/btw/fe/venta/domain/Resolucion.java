package com.btw.fe.venta.domain;

import java.util.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Data;

@Data
@XmlRootElement(name = "resolucionDian")
@XmlAccessorType(XmlAccessType.FIELD)
public class Resolucion {

  @XmlElement(name = "resolucion")
  private String resolucion;
  @XmlElement(name = "vigenciaDesde")
  private Date vigenciaDesde;
  @XmlElement(name = "vigenciaHasta")
  private Date vigenciaHasta;
  @XmlElement(name = "prefijo")
  private String prefijo;
  @XmlElement(name = "inicioConsecutivo")
  private int inicioConsecutivo;
  @XmlElement(name = "finConsecutivo")
  private int finConsecutivo;

}
