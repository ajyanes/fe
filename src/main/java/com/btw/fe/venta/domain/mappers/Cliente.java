package com.btw.fe.venta.domain.mappers;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@IdClass(ClientePk.class)
@Table(name = "ven_cliente")
public class Cliente implements Serializable {

  private int tipopersona;
  private String nombrecomercial;
  private String nombrerazonsocial;
  private String numeroidentificacion;
  private String digitoverificacion;
  private int tipoid;
  private int regimen;
  private String responsabilidadfiscal;
  private String obligaciontributaria;
  @Id
  private String prefijo;
  @Id
  private String numero;

}
