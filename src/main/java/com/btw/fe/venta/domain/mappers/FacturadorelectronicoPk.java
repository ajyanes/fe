package com.btw.fe.venta.domain.mappers;

import java.io.Serializable;

public class FacturadorelectronicoPk implements Serializable {

  private String prefijo;
  private String numero;
}
