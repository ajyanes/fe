package com.btw.fe.venta.domain.mappers;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@IdClass(CodigoItemPk.class)
@Table(name = "ven_codigoitem")
public class CodigoItem implements Serializable {

  private String codigoitem;
  private String estandarcodigoitem;
  @Id
  private String prefix;
  @Id
  private String numero;

}
