package com.btw.fe.venta.domain.mappers;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@IdClass(FormaPagoPk.class)
@Table(name = "ven_formapago")
public class FormaPago implements Serializable {

  private int metodopago;
  private String mediopago;
  private Date fechavencimiento;
  private int idpago;
  @Id
  private String prefix;
  @Id
  private String numero;

}
