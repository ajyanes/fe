package com.btw.fe.venta.domain.cliente;

import com.btw.fe.venta.domain.Contact;
import com.btw.fe.venta.domain.Address;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Data;

@Data
@XmlRootElement(name = "cliente")
@XmlAccessorType(XmlAccessType.FIELD)
public class Customer {

  @XmlElement(name = "tipoPersona")
  private int tipoPersona;
  @XmlElement(name = "nombreComercial")
  private String nombreComercial;
  @XmlElementRef(name = "direccion", type = Address.class)
  private Address address;
  @XmlElement(name = "nombreORazonSocial")
  private String nombreORazonSocial;
  @XmlElement(name = "numeroIdentificacion")
  private String numeroIdentificacion;
  @XmlElement(name = "digitoVerificacion")
  private String digitoVerificacion;
  @XmlElement(name = "tipoId")
  private int tipoId;
  @XmlElement(name = "regimen")
  private int regimen;
  @XmlElement(name = "responsabilidadFiscal")
  private String responsabilidadFiscal;
  @XmlElement(name = "obligacionTributaria")
  private String obligacionTributaria;
  @XmlElementRef(name = "contacto", type = Contact.class)
  private Contact contact;

}
