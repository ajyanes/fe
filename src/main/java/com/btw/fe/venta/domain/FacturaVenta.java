package com.btw.fe.venta.domain;

import com.btw.fe.venta.domain.cliente.Customer;
import com.btw.fe.venta.domain.facturador.Facturador;
import java.util.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Data;

@Data
@XmlRootElement(name = "FacturaVenta")
@XmlAccessorType(XmlAccessType.FIELD)
public class FacturaVenta {

  @XmlElement(name = "prefijo")
  private String prefijo;
  @XmlElement(name = "numero")
  private String numero;
  @XmlElement(name = "versionUBL")
  private String ubl;
  @XmlElement(name = "versionDIAN")
  private String versionDian;
  @XmlElement(name = "tipoOperacion")
  private int tipoOperacion;
  @XmlElement(name = "pais")
  private int pais;
  @XmlElement(name = "fechaExpedicion")
  private Date fechaExp;
  @XmlElement(name = "fechaVencimiento")
  private Date fechaVen;
  @XmlElement(name = "informacionAdicionalDocumento")
  private String infoAdicional;
  @XmlElement(name = "codigoMoneda")
  private String moneda;
  @XmlElement(name = "tipoFactura")
  private String tipoFactura;
  @XmlElement(name = "numeroItems")
  private int items;

  @XmlElementRef(name = "facturadorElectronico", type = Facturador.class)
  private Facturador facturador;

  @XmlElementRef(name = "cliente", type = Customer.class)
  private Customer customer;

  @XmlElementRef(name = "formaPago", type = Payment.class)
  private Payment payment;

  @XmlElementRef(name = "totales", type = Totals.class)
  private Totals totals;

  @XmlElementRef(name = "atributosPSL", type = Psl.class)
  private Psl psl;

  @XmlElementRef(name = "resolucionDian", type = Resolucion.class)
  private Resolucion resolucion;

  @XmlElementRef(name = "detalle", type = Detail.class)
  private Detail detail;


}
