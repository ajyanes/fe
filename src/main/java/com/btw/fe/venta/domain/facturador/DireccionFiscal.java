package com.btw.fe.venta.domain.facturador;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Data;

@Data
@XmlRootElement(name = "direccionFiscal")
@XmlAccessorType(XmlAccessType.FIELD)
public class DireccionFiscal {

  @XmlElement(name = "codigoCiudad")
  private String codigoCiudad;
  @XmlElement(name = "nombreCiudad")
  private String nombreCiudad;
  @XmlElement(name = "codigoPostal")
  private String codigoPostal;
  @XmlElement(name = "codigoDepartamento")
  private String codigoDepartamento;
  @XmlElement(name = "nombreDepartamento")
  private String nombreDepartamento;
  @XmlElement(name = "direccion")
  private String direccion;
  @XmlElement(name = "codigoPais")
  private String codigoPais;

}
