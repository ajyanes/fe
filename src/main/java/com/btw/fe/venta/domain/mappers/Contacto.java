package com.btw.fe.venta.domain.mappers;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "ven_contacto")
public class Contacto implements Serializable {

  private String nombre;
  private String telefono;
  private String correoelectronico;
  @Id
  private String numid;
  private String prefijo;
  private String numero;

}
