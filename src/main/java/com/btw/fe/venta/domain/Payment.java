package com.btw.fe.venta.domain;

import java.util.Date;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Data;

@Data
@XmlRootElement(name = "formaPago")
@XmlAccessorType(XmlAccessType.FIELD)
public class Payment {

  @XmlElement(name = "metodoPago")
  private int metodoPago;
  @XmlElement(name = "medioPago")
  private String medioPago;
  @XmlElement(name = "fechaVencimiento")
  private Date fechaVencimiento;
  @XmlElement(name = "idPago")
  private int idPago;

}
