package com.btw.fe.venta.domain;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Data;

@Data
@XmlRootElement(name = "detalle")
@XmlAccessorType(XmlAccessType.FIELD)
public class Detail {

  @XmlElement(name = "cantidad")
  private int cantidad;
  @XmlElement(name = "unidadMedida")
  private String unidadMedida;
  @XmlElement(name = "precioUnitario")
  private Long precioUnitario;
  @XmlElement(name = "precioTotalNeto")
  private Long precioTotalNeto;
  @XmlElement(name = "descripcion")
  private String descripcion;
  @XmlElementRef(name = "codigoItem", type = Item.class)
  private Item item;
  @XmlElement(name = "cantidadBasePrecio")
  private int cantidadBasePrecio;
  @XmlElement(name = "unidadMedidaBase")
  private String unidadMedidaBase;

}
