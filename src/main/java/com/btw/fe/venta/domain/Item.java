package com.btw.fe.venta.domain;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Data;

@Data
@XmlRootElement(name = "codigoItem")
@XmlAccessorType(XmlAccessType.FIELD)
public class Item {

  @XmlElement(name = "codigoItem")
  private String codigoItem;

  @XmlElement(name = "estandarCodigoItem")
  private String estandarCodigoItem;

}
