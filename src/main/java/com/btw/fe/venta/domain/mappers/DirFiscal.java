package com.btw.fe.venta.domain.mappers;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "ven_direccionfiscal")
public class DirFiscal implements Serializable {

  private String codigociudad;
  private String nombreciudad;
  private String codigopostal;
  private String codigodepartamento;
  private String nombredepartamento;
  private String direccion;
  private String codigopais;
  @Id
  private String numid;
  private String prefijo;
  private String numero;

}
