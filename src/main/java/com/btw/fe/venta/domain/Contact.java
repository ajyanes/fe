package com.btw.fe.venta.domain;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Data;

@Data
@XmlRootElement(name = "contacto")
@XmlAccessorType(XmlAccessType.FIELD)
public class Contact {

  @XmlElement(name = "nombre")
  private String nombre;
  @XmlElement(name = "telefono")
  private String telefono;
  @XmlElement(name = "correoElectronico")
  private String correoElectronico;

}
