package com.btw.fe.venta.domain.mappers;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@IdClass(TotalesPk.class)
@Table(name = "ven_totales")
public class Totales implements Serializable {

  private double valortotalbruto;
  private double basegravable;
  private double valortotalnetoimpuestosincluidos;
  private double valortotaldescuentos;
  private double valortotalcargos;
  private double valortotalanticipos;
  private double valorajustedocumento;
  private double valortotaldocumento;
  @Id
  private String prefix;
  @Id
  private String numero;

}
