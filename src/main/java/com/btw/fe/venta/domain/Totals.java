package com.btw.fe.venta.domain;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Data;

@Data
@XmlRootElement(name = "totales")
@XmlAccessorType(XmlAccessType.FIELD)
public class Totals {

  @XmlElement(name = "valorTotalBruto")
  private double valorTotalBruto;
  @XmlElement(name = "baseGravable")
  private double baseGravable;
  @XmlElement(name = "valorTotalNetoImpuestosIncluidos")
  private double valorTotalNetoImpuestosIncluidos;
  @XmlElement(name = "valorTotalDescuentos")
  private double valorTotalDescuentos;
  @XmlElement(name = "valorTotalCargos")
  private double valorTotalCargos;
  @XmlElement(name = "valorTotalAnticipos")
  private double valorTotalAnticipos;
  @XmlElement(name = "valorAjusteDocumento")
  private double valorAjusteDocumento;
  @XmlElement(name = "valorTotalDocumento")
  private double valorTotalDocumento;

}
