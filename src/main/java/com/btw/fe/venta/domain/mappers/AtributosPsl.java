package com.btw.fe.venta.domain.mappers;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@IdClass(AtributosPslPk.class)
@Table(name = "ven_atributospsl")
public class AtributosPsl implements Serializable {

  private boolean generarpdf;
  private boolean pdfexterno;
  private boolean notificarcliente;
  private String correoelectronicocliente;
  @Id
  private String prefix;
  @Id
  private String numero;

}
