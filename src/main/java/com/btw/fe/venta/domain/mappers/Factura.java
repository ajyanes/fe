package com.btw.fe.venta.domain.mappers;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@IdClass(FacturaPk.class)
@Table(name = "ven_factura")
public class Factura implements Serializable {

  @Id
  @Column(name = "prefijo")
  private String prefijo;
  @Id
  @Column(name = "numero")
  private String numero;
  @Column(name = "ubl")
  private String ubl;
  @Column(name = "versiondian")
  private String versiondian;
  @Column(name = "tipooperacion")
  private int tipooperacion;
  @Column(name = "pais")
  private int pais;
  @Column(name = "fechaexp")
  private Date fechaexp;
  @Column(name = "fechaven")
  private Date fechaven;
  @Column(name = "tipofactura")
  private String tipofactura;
  @Column(name = "infoadicional")
  private String infoadicional;
  @Column(name = "moneda")
  private String moneda;
  @Column(name = "items")
  private int items;

}
