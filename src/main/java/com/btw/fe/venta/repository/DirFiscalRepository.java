package com.btw.fe.venta.repository;

import com.btw.fe.venta.domain.mappers.DirFiscal;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DirFiscalRepository extends CrudRepository<DirFiscal, Long> {

}
