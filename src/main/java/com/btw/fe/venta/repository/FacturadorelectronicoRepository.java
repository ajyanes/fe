package com.btw.fe.venta.repository;

import com.btw.fe.venta.domain.mappers.Facturadorelectronico;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FacturadorelectronicoRepository extends
    CrudRepository<Facturadorelectronico, Long> {

}
