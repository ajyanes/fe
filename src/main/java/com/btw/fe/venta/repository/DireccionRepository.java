package com.btw.fe.venta.repository;

import com.btw.fe.venta.domain.mappers.Direccion;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DireccionRepository extends CrudRepository<Direccion, Long> {

}
