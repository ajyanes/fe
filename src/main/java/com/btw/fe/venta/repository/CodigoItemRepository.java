package com.btw.fe.venta.repository;

import com.btw.fe.venta.domain.mappers.CodigoItem;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CodigoItemRepository extends CrudRepository<CodigoItem, Long> {

}
