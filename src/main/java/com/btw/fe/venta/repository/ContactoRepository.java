package com.btw.fe.venta.repository;

import com.btw.fe.venta.domain.mappers.Contacto;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContactoRepository extends CrudRepository<Contacto, Long> {

}
