package com.btw.fe.venta.repository;

import com.btw.fe.venta.domain.mappers.Detalle;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DetalleRepository extends CrudRepository<Detalle, Long> {

}
