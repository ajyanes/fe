package com.btw.fe.venta.repository;

import com.btw.fe.venta.domain.mappers.ResolucionDian;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ResolucionDianRepository extends CrudRepository<ResolucionDian, Long> {

}
