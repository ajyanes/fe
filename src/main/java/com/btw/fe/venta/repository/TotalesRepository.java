package com.btw.fe.venta.repository;

import com.btw.fe.venta.domain.mappers.Totales;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TotalesRepository extends CrudRepository<Totales, Long> {

}
