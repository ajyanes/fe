package com.btw.fe.venta.repository;

import com.btw.fe.venta.domain.mappers.Cliente;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClienteRepository extends CrudRepository<Cliente, Long> {

}
