package com.btw.fe.venta.repository;

import com.btw.fe.venta.domain.mappers.Factura;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FacturaRepository extends CrudRepository<Factura, Long> {

}
