package com.btw.fe.venta.repository;

import com.btw.fe.venta.domain.mappers.AtributosPsl;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AtributosPslRepository extends CrudRepository<AtributosPsl, Long> {

}
