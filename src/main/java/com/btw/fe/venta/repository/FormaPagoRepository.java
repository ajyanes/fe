package com.btw.fe.venta.repository;

import com.btw.fe.venta.domain.mappers.FormaPago;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FormaPagoRepository extends CrudRepository<FormaPago, Long> {

}
