package com.btw.fe.facturaelectronica.domain.models;

import com.btw.fe.facturaelectronica.modules.AdditionalCustomer.model.AdditionalCustomerDTO;
import com.btw.fe.facturaelectronica.modules.COOneTime.model.COOneTimeDTO;
import com.btw.fe.facturaelectronica.modules.PrepaidPayment.model.PrepaidPaymentDTO;
import com.btw.fe.facturaelectronica.modules.company.model.CompanyDTO;
import com.btw.fe.facturaelectronica.modules.customer.model.CustomerDTO;
import com.btw.fe.facturaelectronica.modules.invcDtl.model.InvcDtlDTO;
import com.btw.fe.facturaelectronica.modules.invcHead.model.InvcHeadDTO;
import com.btw.fe.facturaelectronica.modules.invcMisc.model.InvcMiscDTO;
import com.btw.fe.facturaelectronica.modules.invcTax.model.InvcTaxDTO;
import com.btw.fe.facturaelectronica.modules.salesTRC.model.SalesTRCDTO;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class InvoiceDataSet {

  private InvcHeadDTO invcHead;
  private List<InvcDtlDTO> invcDtl;
  private List<InvcTaxDTO> invcTax;
  private List<InvcMiscDTO> invcMisc;
  private CompanyDTO company;
  private List<CustomerDTO> customer;
  private List<SalesTRCDTO> salesTRC;
  private List<COOneTimeDTO> coOneTime;
  private PrepaidPaymentDTO prepaidPayment;
  private List<AdditionalCustomerDTO> additionalCustomers;

}
