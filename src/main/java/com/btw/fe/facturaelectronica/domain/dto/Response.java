package com.btw.fe.facturaelectronica.domain.dto;

import com.btw.fe.facturaelectronica.modules.invcHead.model.InvcHeadDTO;
import java.util.List;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class Response {

  private String message;
  private List<InvcHeadDTO> invcHeadDTOS;
}
