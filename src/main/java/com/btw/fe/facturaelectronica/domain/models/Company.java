package com.btw.fe.facturaelectronica.domain.models;

import lombok.Data;

@Data
public class Company {

  private String company;
  private String stateTaxID;
  private String name;
  private String regimeTypeC;
  private String fiscalResposabilityC;
  private String operationTypeC;
  private String attrOperationTypeC;
  private String companyTypeC;
  private String state;
  private String stateNum;
  private String city;
  private String cityNum;
  private String industryClassificationCodeC;
  private String identificationType;
  private String address1;
  private String country;
  private String postalZoneC;
  private String phoneNum;
  private String faxNum;
  private String email;
  private String webPage;
}
