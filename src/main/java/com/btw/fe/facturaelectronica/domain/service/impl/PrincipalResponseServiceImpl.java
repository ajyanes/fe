package com.btw.fe.facturaelectronica.domain.service.impl;

import com.btw.fe.facturaelectronica.domain.models.ARInvoiceDataSet;
import com.btw.fe.facturaelectronica.domain.models.PrincipalResponse;
import com.btw.fe.facturaelectronica.domain.service.PrincipalResponseService;
import com.btw.fe.facturaelectronica.modules.AdditionalCustomer.service.AdditionalCustomerService;
import com.btw.fe.facturaelectronica.modules.COOneTime.service.COOneTimeService;
import com.btw.fe.facturaelectronica.modules.InvcDisc.service.InvcDiscService;
import com.btw.fe.facturaelectronica.modules.PrepaidPayment.service.PrepaidPaymentService;
import com.btw.fe.facturaelectronica.modules.company.service.CompanyService;
import com.btw.fe.facturaelectronica.modules.customer.service.CustomerService;
import com.btw.fe.facturaelectronica.modules.invcDtl.service.InvcDtlService;
import com.btw.fe.facturaelectronica.modules.invcHead.model.InvcHeadDTO;
import com.btw.fe.facturaelectronica.modules.invcHead.model.InvoiceHead;
import com.btw.fe.facturaelectronica.modules.invcHead.service.InvcHeadService;
import com.btw.fe.facturaelectronica.modules.invcMisc.service.InvcMiscService;
import com.btw.fe.facturaelectronica.modules.invcTax.service.InvcTaxService;
import com.btw.fe.facturaelectronica.modules.salesTRC.service.SalesTRCService;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PrincipalResponseServiceImpl implements PrincipalResponseService {

  @Autowired
  private InvcHeadService invcHeadService;
  @Autowired
  private InvcDtlService invcDtlService;
  @Autowired
  private InvcTaxService invcTaxService;
  @Autowired
  private InvcMiscService invcMiscService;
  @Autowired
  private CompanyService companyService;
  @Autowired
  private CustomerService customerService;
  @Autowired
  private SalesTRCService salesTRCService;
  @Autowired
  private COOneTimeService coOneTimeService;
  @Autowired
  private PrepaidPaymentService prepaidPaymentService;
  @Autowired
  private InvcDiscService invcDiscService;
  @Autowired
  private AdditionalCustomerService additionalCustomerService;

  private List<ARInvoiceDataSet> arInvoice;

  public PrincipalResponseServiceImpl() {
    arInvoice = new ArrayList<>();
  }

  public PrincipalResponse getValues(String fecExp) {
//        ARInvoiceDataSet arInvoiceDataSet = ARInvoiceDataSet.builder()
//                .invcHead(invcHeadService.getValues(fecExp).stream().collect(Collectors.toList()))
//                .invcDtl(invcDtlService.getValues(fecExp))
//                .invcTax(invcTaxService.getValues(fecExp))
//                .invcMisc(invcMiscService.getValues(fecExp))
//                .company(companyService.getValues(fecExp))
//                .customer(customerService.getValues(fecExp).stream().findAny().orElse(new CustomerDTO()))
//                .salesTRC(salesTRCService.getValues(fecExp))
//                .coOneTime(coOneTimeService.getValues(fecExp).stream().findAny().orElse(new COOneTimeDTO()))
//                .prepaidPayment(prepaidPaymentService.getValues(fecExp).stream().findAny().orElse(new PrepaidPaymentDTO()))
//                .additionalCustomers(additionalCustomerService.getValues(fecExp))
//                .build();
//        List<ARInvoiceDataSet> arInvoiceDataSets = new ArrayList<>();
//        arInvoiceDataSets.add(arInvoiceDataSet);
//        return new PrincipalResponse(arInvoiceDataSets);
    return new PrincipalResponse(null);
  }

  public PrincipalResponse getInvoice(String fecExp) {
    List<ARInvoiceDataSet> arInvoice = new ArrayList<>();

    List<InvcHeadDTO> listInvoices = invcHeadService.getValues(fecExp);

    extracted(arInvoice, listInvoices);

    return new PrincipalResponse(arInvoice);
  }

  @Override
  public PrincipalResponse getInvoiceByState(String state) {
    List<InvcHeadDTO> listInvoices = invcHeadService.getValuesByState(state);

    extracted(arInvoice, listInvoices);

    return new PrincipalResponse(arInvoice);
  }

  @Override
  public PrincipalResponse getInvoiceByDateRange(String startDate, String endDate) {
    arInvoice = new ArrayList<>();
    List<InvcHeadDTO> listInvoices = invcHeadService.getValuesByDateRange(startDate, endDate);

    extracted(arInvoice, listInvoices);

    return new PrincipalResponse(arInvoice);
  }

  @Override
  public PrincipalResponse getInvoiceByFilter(String startDate, String endDate, String docType,
      String invoice, String legalNumber, String mark) {
    arInvoice = new ArrayList<>();

    String result = invcHeadService.validateRange(startDate,endDate);

    if(result==null){
      return new PrincipalResponse(new ArrayList<>());
    }else {

      List<InvcHeadDTO> listInvoices = invcHeadService.getValuesByFilter(startDate, endDate,
          docType,
          invoice, legalNumber);

      extracted(arInvoice, listInvoices);

      if (mark.equals("true")) {
        updateLecturaFe(listInvoices);
      }

      if(arInvoice.size()>0){
        return new PrincipalResponse(arInvoice);
      }else{
        return new PrincipalResponse(arInvoice);
      }


    }
  }

  @Override
  public PrincipalResponse getAllInvoiceByFilter(String startDate, String endDate, String docType,
      String invoice, String legalNumber) {
    arInvoice = new ArrayList<>();
    List<InvcHeadDTO> listInvoices = invcHeadService.getValuesByFilter(startDate, endDate, docType,
        invoice, legalNumber);

    extracted(arInvoice, listInvoices);

    return new PrincipalResponse(arInvoice);
  }

  private void extracted(List<ARInvoiceDataSet> arInvoice, List<InvcHeadDTO> listInvoices) {
    for (InvcHeadDTO invDTO : listInvoices) {
      InvoiceHead invoiceHead = convertInvoice(invDTO);

      ARInvoiceDataSet arInvoiceDataSet = ARInvoiceDataSet.builder()
          .invcHead(invoiceHead)
          .invcDtl(invcDtlService
              .getValues(invoiceHead.getInvoicenum().toString(), invDTO.getEdvtipoconsclie()))
          .invcTax(invoiceHead.getInvoicetype().equals("InvoiceType") ? invcTaxService
              .getValues(invoiceHead.getInvoicenum().toString(), invDTO.getEdvtipoconsclie())
              : new ArrayList<>())
          .invcMisc(invcMiscService
              .getValues(invoiceHead.getInvoicenum().toString(), invDTO.getEdvtipoconsclie())
              .stream().filter(miscDTO -> !miscDTO.getMisccode().isEmpty())
              .collect(Collectors.toList()))
          .company(companyService.getValues(invDTO.getCompany()))
          .customer(customerService
              .getValues(invoiceHead.getInvoicenum().toString(), invDTO.getEdvtipoconsclie()))
          .salesTRC(invoiceHead.getInvoicetype().equals("InvoiceType") ? salesTRCService
              .getValues(invoiceHead.getInvoicenum().toString(), invDTO.getEdvtipoconsclie())
              : new ArrayList<>())
          .COOneTime(coOneTimeService
              .getValues(invoiceHead.getInvoicenum().toString(), invDTO.getEdvtipoconsclie()))
          .prepaidPayment(prepaidPaymentService.getValues(invDTO.getEdvtipoconsclie(),
              invoiceHead.getInvoicenum().toString()).getPrepaidPaymentID_C() == null
              ? new ArrayList<>() : Arrays.asList(
              prepaidPaymentService.getValues(invDTO.getEdvtipoconsclie(),
                  invoiceHead.getInvoicenum().toString())))
          .invcDisc(invcDiscService.getValues(invoiceHead.getInvoicenum().toString(),
              invDTO.getEdvtipoconsclie()).getDiscbaseamount() == 0L ? new ArrayList<>()
              : Arrays.asList(invcDiscService.getValues(invoiceHead.getInvoicenum().toString(),
                  invDTO.getEdvtipoconsclie())))
          .additionalCustomer(additionalCustomerService
              .getValues(invoiceHead.getInvoicenum().toString(), invDTO.getEdvtipoconsclie()).size()>1 ?
              additionalCustomerService
                  .getValues(invoiceHead.getInvoicenum().toString(), invDTO.getEdvtipoconsclie()) : new ArrayList<>())
          .build();

      arInvoice.add(arInvoiceDataSet);
    }
  }

  public InvoiceHead convertInvoice(InvcHeadDTO invcHeadDTO) {
    return InvoiceHead.builder()
        .company(invcHeadDTO.getCompany())
        .invoicetype(invcHeadDTO.getInvoicetype())
        .invoicenum(invcHeadDTO.getInvoicenum())
        .legalnumber(invcHeadDTO.getLegalnumber())
        .invoiceref(invcHeadDTO.getInvoiceref())
        .invoicerefcufe(invcHeadDTO.getInvoicerefcufe())
        .custnum(invcHeadDTO.getCustnum())
        .contactname(invcHeadDTO.getContactname())
        .customername(invcHeadDTO.getCustomername())
        .invoicedate(invcHeadDTO.getInvoicedate())
        .duedate(invcHeadDTO.getDuedate())
        .dspdocsubtotal(invcHeadDTO.getDspdocsubtotal())
        .doctaxamt(invcHeadDTO.getDoctaxamt())
        .docwhtaxamt(invcHeadDTO.getDocwhtaxamt())
        .dspdocinvoiceamt(invcHeadDTO.getDspdocinvoiceamt())
        .invoicecomment(invcHeadDTO.getInvoicecomment())
        .currencycodecurrencyid(invcHeadDTO.getCurrencycodecurrencyid())
        .currencycode(invcHeadDTO.getCurrencycode())
        .discount(invcHeadDTO.getDiscount())
        .resolution1(invcHeadDTO.getResolution1())
        .cmReasonCode_C(invcHeadDTO.getCmReasonCode_C())
        .cmReasonDesc_C(invcHeadDTO.getCmReasonDesc_C())
        .dmReasonCode_C(invcHeadDTO.getDmReasonCode_C())
        .dmReasonDesc_C(invcHeadDTO.getDmReasonDesc_C())
        .paymentMeansID_C(invcHeadDTO.getPaymentMeansID_C())
        .paymentmeansdescription(invcHeadDTO.getPaymentmeansdescription())
        .paymentMeansCode_C(invcHeadDTO.getPaymentMeansCode_C())
        .paymentdurationmeasure(invcHeadDTO.getPaymentdurationmeasure())
        .paymentduedate(invcHeadDTO.getPaymentduedate())
        .calculationRate_C(invcHeadDTO.getCalculationRate_C())
        .datecalculationRate_C(invcHeadDTO.getDatecalculationRate_C())
        .zipFileBase64_C(invcHeadDTO.getZipFileBase64_C())
        .character01(invcHeadDTO.getCharacter01())
        .character02(invcHeadDTO.getCharacter02())
        .character03(invcHeadDTO.getCharacter03())
        .character04(invcHeadDTO.getCharacter04())
        .character05(invcHeadDTO.getCharacter05())
        .startPeriodDate(invcHeadDTO.getStartPeriodDate())
        .endPeriodDate(invcHeadDTO.getEndPerioddate())
        .authorizationnumber(invcHeadDTO.getAuthorizationnumber())
        .policynumber(invcHeadDTO.getPolicynumber())
        .doctorname(invcHeadDTO.getDoctorname())
        .doctorspecialty(invcHeadDTO.getDoctorspecialty())
        .moderatingfeeamt(invcHeadService.getCuotaModerado(invcHeadDTO.getTipocons()+invcHeadDTO.getInvoicenum()))
        .copaymentamt(invcHeadService.getCopago(invcHeadDTO.getTipocons()+invcHeadDTO.getInvoicenum()))
        .retrievalfeeamt(invcHeadService.getCuotaRecuperacion(invcHeadDTO.getTipocons()+invcHeadDTO.getInvoicenum()))
        .sharedpaymentamt(invcHeadService.getPagosCompartidos(invcHeadDTO.getTipocons()+invcHeadDTO.getInvoicenum()))
        .serviceprovidercode(invcHeadDTO.getServiceprovidercode())
        .mipresnumber(invcHeadDTO.getMipresnumber())
        .mipresid(invcHeadDTO.getMipresid())
        .contractnumber(invcHeadService.getContrato(invcHeadDTO.getTipocons()+invcHeadDTO.getInvoicenum()))
        .codoperacionrecaudo(invcHeadDTO.getCodoperacionrecaudo())
        .cantidadacreditar(invcHeadDTO.getCantidadacreditar())
        .conceptorecaudo(invcHeadDTO.getConceptorecaudo())
        .build();

  }

  public void updateLecturaFe(List<InvcHeadDTO> invcHeadDTOS) {
    List<InvcHeadDTO> invcHeadDTOList = new ArrayList<>();
    for (InvcHeadDTO invcHeadDTO : invcHeadDTOS) {
      InvcHeadDTO headDTO = InvcHeadDTO.builder()
          .company(invcHeadDTO.getCompany())
          .edvtipoconsclie(invcHeadDTO.getEdvtipoconsclie())
          .invoicenum(invcHeadDTO.getInvoicenum())
          .lecturafe(1)
          .build();

      invcHeadDTOList.add(headDTO);
    }
    invcHeadService.updateLecturaFe(invcHeadDTOList);
  }

}
