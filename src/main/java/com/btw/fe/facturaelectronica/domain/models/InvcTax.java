package com.btw.fe.facturaelectronica.domain.models;

import lombok.Data;

@Data
public class InvcTax {

  private String company;
  private String invoiceNum;
  private String invoiceLine;
  private String currencyCode;
  private String rateCode;
  private String docTaxableAmt;
  private String taxAmt;
  private String docTaxAmt;
  private String percent;
  private String withholdingTaxC;
  private String baseUnitMeasureC;
  private String perUnitAmountC;
}
