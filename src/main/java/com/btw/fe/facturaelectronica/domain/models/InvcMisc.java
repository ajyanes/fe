package com.btw.fe.facturaelectronica.domain.models;

public class InvcMisc {

  private String company;
  private String invoiceNum;
  private Long invoiceLine;
  private String miscCode;
  private String description;
  private String miscAmt;
  private String docMiscAmt;
  private String miscCodeDescription;
  private String percentage;
  private String miscBaseAmt;
}
