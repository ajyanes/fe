package com.btw.fe.facturaelectronica.domain.dto;


import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class InvoicesDto {

  private String edvnumedocuclie;
  private String edvtipoconsclie;
  private String edvcompania;
  private String edvtipodocuclie;
  private String edvtipoconspedi;
  private String edvnumepedi;
  private String edvtipoconsremi;
  private String edvnumeremi;
  private String edvcliente;
  private String edvestabclien;
  private String edvtasacamb;
  private String edvtotamonenego;
  private String edvcomentario;
  private String edvfechexpe;
  private String edvfechvenc;
  private String edvvalobruto;
  private String edvvaloneto;
  private String edvvalodescpiepag;
  private String edvtotaiva;
  private String edvtotareteiva;
  private String edvtotaretefuen;
  private String edvtotalcargo;
  private String edvvendedor;
  private String edvclasdocuclie;
  private String edvdivision;
  private String edvconcepto;
  private String edvmoneda;
  private String edvtotamoneloca;
  private String eobcodigo;
  private String eobusuario;
  private String edvformapago;
  private String edvplazo;
  private String edvporcdescpiepag;
  private String edvvalodescpp1;
  private String edvporcdescpp1;
  private String edvvalodescpp2;
  private String edvporcdescpp2;
  private String edvvalodescpp3;
  private String edvporcdescpp3;
  private String edvfechvencdescpp1;
  private String edvfechvencdescpp2;
  private String edvfechvencdescpp3;
  private String edvtotaimpucons;
  private String edvtotaotroimpu;
  private String edvtotareteotroimpu;
  private String edvtotaotrarete;
  private String edvcomencorto;
  private String edvplazdpp1;
  private String edvplazdpp2;
  private String edvplazdpp3;
  private String edvcobrador;
  private String edvsignodocu;
  private String edvcoddpp;
  private String edvimpreso;
  private String edvtotaretfte;
  private String edvtotareteica;
  private String edvtotalrie;
  private String edvtotalretrie;
  private String edvavisorecib;
  private String edvordenentre;
  private String edvcodigoqr;
  private String edvcodigocufe;
  private String edvprefijo;
  private String edvfechaexpfe;
  private String edvnumerresol;
  private String edvhoraexpe;
  private String edvordencompra;

}
