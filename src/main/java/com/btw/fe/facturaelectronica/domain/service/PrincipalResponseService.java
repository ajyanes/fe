package com.btw.fe.facturaelectronica.domain.service;

import com.btw.fe.facturaelectronica.domain.models.PrincipalResponse;

public interface PrincipalResponseService {

  PrincipalResponse getValues(String fecExp);

  PrincipalResponse getInvoice(String fecExp);

  PrincipalResponse getInvoiceByState(String state);

  PrincipalResponse getInvoiceByDateRange(String startDate, String endDate);

  PrincipalResponse getInvoiceByFilter(String startDate, String endDate, String docType,
      String invoice, String legalNumber, String mark);

  PrincipalResponse getAllInvoiceByFilter(String startDate, String endDate, String docType,
      String invoice, String legalNumber);

}
