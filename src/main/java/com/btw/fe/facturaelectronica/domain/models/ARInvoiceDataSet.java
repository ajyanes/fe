package com.btw.fe.facturaelectronica.domain.models;

import com.btw.fe.facturaelectronica.modules.AdditionalCustomer.model.AdditionalCustomerDTO;
import com.btw.fe.facturaelectronica.modules.COOneTime.model.COOneTimeDTO;
import com.btw.fe.facturaelectronica.modules.InvcDisc.model.InvcDiscDTO;
import com.btw.fe.facturaelectronica.modules.PrepaidPayment.model.PrepaidPaymentDTO;
import com.btw.fe.facturaelectronica.modules.company.model.CompanyDTO;
import com.btw.fe.facturaelectronica.modules.customer.model.CustomerDTO;
import com.btw.fe.facturaelectronica.modules.invcDtl.model.InvoiceDetail;
import com.btw.fe.facturaelectronica.modules.invcHead.model.InvoiceHead;
import com.btw.fe.facturaelectronica.modules.invcMisc.model.InvcMiscDTO;
import com.btw.fe.facturaelectronica.modules.invcTax.model.InvcTaxDTO;
import com.btw.fe.facturaelectronica.modules.salesTRC.model.SalesTRCDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ARInvoiceDataSet {

  private InvoiceHead invcHead;
  private List<InvoiceDetail> invcDtl;
  private List<InvcTaxDTO> invcTax;
  private List<InvcMiscDTO> invcMisc;
  private CompanyDTO company;
  private CustomerDTO customer;
  private List<SalesTRCDTO> salesTRC;
  private COOneTimeDTO COOneTime;
  private List<PrepaidPaymentDTO> prepaidPayment;
  private List<InvcDiscDTO> invcDisc;
  private List<AdditionalCustomerDTO> additionalCustomer;

}
