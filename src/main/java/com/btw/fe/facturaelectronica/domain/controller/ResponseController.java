package com.btw.fe.facturaelectronica.domain.controller;

import com.btw.fe.facturaelectronica.domain.models.PrincipalResponse;
import com.btw.fe.facturaelectronica.domain.service.PrincipalResponseService;
import com.btw.fe.venta.domain.mappers.Factura;
import com.btw.fe.venta.service.SaleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;


@RestController
public class ResponseController {

  @Autowired
  private PrincipalResponseService service;

  @Autowired
  private SaleService saleService;

  @GetMapping("/show")
  public ResponseEntity<PrincipalResponse> getValues(@RequestParam String date) {
    return new ResponseEntity<>(service.getInvoice(date), HttpStatus.ACCEPTED);
  }

  @GetMapping("/showbystate")
  public ResponseEntity<PrincipalResponse> getValuesByState(@RequestParam String stateInvoice) {
    return new ResponseEntity<>(service.getInvoiceByState(stateInvoice), HttpStatus.ACCEPTED);
  }

  @GetMapping("/showbydaterange")
  public ResponseEntity<PrincipalResponse> getValuesByDateRange(@RequestParam String startDate,
      @RequestParam String endDate) {
    return new ResponseEntity<>(service.getInvoiceByDateRange(startDate, endDate),
        HttpStatus.ACCEPTED);
  }

  @GetMapping("/invoices")
  public ResponseEntity<PrincipalResponse> getValuesAll(@RequestParam String startDate,
      @RequestParam String endDate, @RequestParam String docType,
      @RequestParam String invoiceNumber, @RequestParam String legalNumber,
      @RequestParam String mark) {

    PrincipalResponse response = service.getInvoiceByFilter(startDate, endDate, docType, invoiceNumber, legalNumber, mark);

    if(response != null){
      return new ResponseEntity<>(response,HttpStatus.OK);
    }else{
      return new ResponseEntity<>(response,HttpStatus.NOT_FOUND);
    }

  }

  @GetMapping("/invoicesbyfilter")
  public ResponseEntity<PrincipalResponse> getValuesFilter(@RequestParam String startDate,
      @RequestParam String endDate, @RequestParam String docType,
      @RequestParam String invoiceNumber, @RequestParam String legalNumber) {
    return new ResponseEntity<>(
        service.getAllInvoiceByFilter(startDate, endDate, docType, invoiceNumber, legalNumber),
        HttpStatus.ACCEPTED);
  }

  @PostMapping("/loadFile")
  @ResponseBody
  public ResponseEntity<Factura> loadFile(@RequestParam("file") MultipartFile file) {
    return new ResponseEntity<>(saleService.loadFile(file), HttpStatus.ACCEPTED);
  }


}
