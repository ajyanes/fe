package com.btw.fe.facturaelectronica.domain.models;

import lombok.Data;

@Data
public class InvcDtl {

  private String company;
  private String invoiceNum;
  private String invoiceType;
  private String invoiceLine;
  private String partNum;
  private String lineDesc;
  private String lineDesc2;
  private String lineDesc3;
  private String partNumPartDescription;
  private String sellingShipQty;
  private String salesUM;
  private String unitPrice;
  private String docUnitPrice;
  private String docEXTPrice;
  private String dspDocEXTPrice;
  private String discountPercent;
  private String discount;
  private String docDiscount;
  private String dspDocLessDiscount;
  private String dspDocTotalMiscChrg;
  private String currencyCode;
  private String codInvima;
  private String poLine;
  private String character01;
  private String character02;
  private String character03;
  private String character04;
  private String character05;
  private String standardItemID;
  private String schemeAgencyID;
}
