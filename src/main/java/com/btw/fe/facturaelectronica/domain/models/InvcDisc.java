package com.btw.fe.facturaelectronica.domain.models;

public class InvcDisc {

  private String company;
  private String invoiceNum;
  private String discCode;
  private String description;
  private Long discBaseAmount;
  private Long discAmt;
  private Long docDiscAmt;
  private String discCodeDescription;
  private Long percentage;
}
