package com.btw.fe.facturaelectronica.domain.models;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PrincipalResponse {

  private List<ARInvoiceDataSet> arInvoiceDataSet;

}
