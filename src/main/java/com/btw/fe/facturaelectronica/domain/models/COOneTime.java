package com.btw.fe.facturaelectronica.domain.models;

import lombok.Data;

@Data
public class COOneTime {

  private String company;
  private String identificationType;
  private String COOneTimeID;
  private String companyName;
  private String countryCode;
}
