package com.btw.fe.facturaelectronica.domain.models;

import lombok.Data;

@Data
public class SalesTRC {

  private String company;
  private String rateCode;
  private String taxCode;
  private String description;
  private String idImpDIAN_C;
  private String totalValueTaxRenta;
}
