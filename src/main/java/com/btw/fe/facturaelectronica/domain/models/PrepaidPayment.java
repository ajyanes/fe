package com.btw.fe.facturaelectronica.domain.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class PrepaidPayment {

  @Id
  private String prepaidPaymentIDC;
  private String paidDate;
  private String paidTime;
  private String paidAmount;
  private String instructionPrepaidPaymentC;
}
