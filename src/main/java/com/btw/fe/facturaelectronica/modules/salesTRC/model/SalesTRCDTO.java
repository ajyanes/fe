package com.btw.fe.facturaelectronica.modules.salesTRC.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class SalesTRCDTO {

  private String company;
  @Id
  private String ratecode;
  private String taxcode;
  private String description;
  @Column(name = "idimpdianc")
  private String idimpdian_c;
  private String totalvaluetaxrenta;
}
