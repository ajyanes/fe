package com.btw.fe.facturaelectronica.modules.invcHead.repository.impl;

import com.btw.fe.facturaelectronica.modules.invcHead.model.InvcHeadDTO;
import com.btw.fe.facturaelectronica.modules.invcHead.repository.InvcHeadRepositoryCustom;
import com.btw.fe.facturaelectronica.util.Utils;
import java.math.BigDecimal;
import java.sql.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

@Repository
@PropertySource("classpath:sentences.xml")
public class InvcHeadRepositoryImpl implements InvcHeadRepositoryCustom {

  @Autowired
  EntityManager em;

  @Autowired
  private Environment env;


  @Override
  public List<InvcHeadDTO> findByEdvfechexp(String fecExp) {
    String query = env.getProperty("findInvcHead.sql");
    Query nativeQuery = em.createNativeQuery(query, InvcHeadDTO.class);
    nativeQuery.setParameter("compania", "01");
    nativeQuery.setParameter("year", Utils.getYear(fecExp));
    nativeQuery.setParameter("period", Utils.getPeriod(fecExp));
    nativeQuery.setParameter("findDate", fecExp);
    nativeQuery.setHint("org.hibernate.fetchSize",5000);

    List<InvcHeadDTO> result = nativeQuery.getResultList();
    return result;
  }

  @Override
  public List<InvcHeadDTO> findByState(String state) {
    String query = env.getProperty("findInvcHeadByState.sql");
    Query nativeQuery = em.createNativeQuery(query, InvcHeadDTO.class);
    nativeQuery.setParameter("compania", "01");
    nativeQuery.setParameter("state", state);
    nativeQuery.setHint("org.hibernate.fetchSize",5000);

    List<InvcHeadDTO> result = nativeQuery.getResultList();
    return result;
  }

  @Override
  public List<InvcHeadDTO> findByDateRange(String startDate, String endDate) {
    String query = env.getProperty("findInvcHeadBetwenDates.sql");
    Query nativeQuery = em.createNativeQuery(query, InvcHeadDTO.class);
    nativeQuery.setParameter("compania", "01");
    nativeQuery.setParameter("year", Utils.getYear(startDate));
    nativeQuery.setParameter("period", Utils.getPeriod(startDate));
    nativeQuery.setParameter("startDate", startDate);
    nativeQuery.setParameter("endDate", endDate);
    nativeQuery.setHint("org.hibernate.fetchSize",5000);

    System.out.println("QUERY: "+ query);

    List<InvcHeadDTO> result = nativeQuery.getResultList();
    return result;
  }

  @Override
  public List<InvcHeadDTO> findByApplyFilters(StringBuilder qry) {
    String query = env.getProperty("findSingleInvc.sql");
    query += qry;
    Query nativeQuery = em.createNativeQuery(query, InvcHeadDTO.class);
    nativeQuery.setParameter("compania", "01");
    nativeQuery.setHint("org.hibernate.fetchSize",500);

    System.out.println("QUERY 1: "+ query);

    List<InvcHeadDTO> result = nativeQuery.getResultList();
    return result;
  }

  @Override
  public List<String> getDocuments() {
    String query = env.getProperty("documents.sql");
    Query nativeQuery = em.createNativeQuery(query);

    return nativeQuery.getResultList();
  }

  @Override
  public void updateMarkInvoice(List<InvcHeadDTO> invcHeadDTOS) {
    String query = env.getProperty("updateMark.sql");
    Query nativeQuery = em.createNativeQuery(query, InvcHeadDTO.class);
    nativeQuery.setParameter("compania", "01");

    for (InvcHeadDTO invcHeadDTO : invcHeadDTOS) {
      nativeQuery.setParameter("valor", invcHeadDTO.getLecturafe());
      nativeQuery.setParameter("doc", invcHeadDTO.getEdvtipoconsclie());
      nativeQuery.setParameter("num", invcHeadDTO.getInvoicenum());

      nativeQuery.executeUpdate();
    }
  }

  @Override
  public List<Date> fechaInicio(String doc) {
    String query = env.getProperty("fechaInicio.sql");
    Query nativeQuery = em.createNativeQuery(query);
    nativeQuery.setParameter("doc", doc);

    List<Date> result = nativeQuery.getResultList();
    return result;
  }

  @Override
  public List<Date> fechaFin(String doc) {
    String query = env.getProperty("fechaFinal.sql");
    Query nativeQuery = em.createNativeQuery(query);
    nativeQuery.setParameter("doc", doc);

    List<Date> result = nativeQuery.getResultList();
    return result;
  }

  @Override
  public List<BigDecimal> cuotaModerado(String doc) {
    String query = env.getProperty("cuotaModerado.sql");
    Query nativeQuery = em.createNativeQuery(query);
    nativeQuery.setParameter("doc", doc);

    List<BigDecimal> result = nativeQuery.getResultList();
    return result;
  }

  @Override
  public List<BigDecimal> copago(String doc) {
    String query = env.getProperty("copago.sql");
    Query nativeQuery = em.createNativeQuery(query);
    nativeQuery.setParameter("doc", doc);

    List<BigDecimal> result = nativeQuery.getResultList();
    return result;
  }

  @Override
  public List<BigDecimal> cuotaRecuperacion(String doc) {
    String query = env.getProperty("cuotaRecuperacion.sql");
    Query nativeQuery = em.createNativeQuery(query);
    nativeQuery.setParameter("doc", doc);

    List<BigDecimal> result = nativeQuery.getResultList();
    return result;
  }

  @Override
  public List<String> pagosCompartidos(String doc) {
    String query = env.getProperty("pagosCompartidos.sql");
    Query nativeQuery = em.createNativeQuery(query);
    nativeQuery.setParameter("doc", doc);

    List<String> result = nativeQuery.getResultList();
    return result;
  }

  @Override
  public List<String> contrato(String doc) {
    String query = env.getProperty("contrato.sql");
    Query nativeQuery = em.createNativeQuery(query);
    nativeQuery.setParameter("doc", doc);

    List<String> result = nativeQuery.getResultList();
    return result;
  }


}
