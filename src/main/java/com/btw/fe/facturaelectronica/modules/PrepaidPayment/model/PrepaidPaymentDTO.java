package com.btw.fe.facturaelectronica.modules.PrepaidPayment.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class PrepaidPaymentDTO {

  @Id
  @Column(name = "prepaidpaymentID_c")
  private String prepaidPaymentID_C;
  private String paiddate;
  private String paidtime;
  private String paidamount;
  @Column(name = "instructionprepaidpayment_c")
  private String instructionprepaidpayment_C;
}
