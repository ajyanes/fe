package com.btw.fe.facturaelectronica.modules.company.service;

import com.btw.fe.facturaelectronica.modules.company.model.CompanyDTO;

public interface CompanyService {

  CompanyDTO getValues(String fecExp);
}
