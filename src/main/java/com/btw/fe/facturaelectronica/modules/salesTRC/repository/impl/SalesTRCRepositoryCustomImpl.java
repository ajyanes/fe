package com.btw.fe.facturaelectronica.modules.salesTRC.repository.impl;

import com.btw.fe.facturaelectronica.modules.salesTRC.model.SalesTRCDTO;
import com.btw.fe.facturaelectronica.modules.salesTRC.repository.SalesTRCRepositoryCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

@Repository
@PropertySource("classpath:sentences.xml")
public class SalesTRCRepositoryCustomImpl implements SalesTRCRepositoryCustom {

  @Autowired
  private EntityManager em;

  @Autowired
  private Environment env;

  public List<SalesTRCDTO> findByFechexp(String nume, String doc) {
    String query = env.getProperty("findSalesTRC.sql");
    Query nativeQuery = em.createNativeQuery(query, SalesTRCDTO.class);
    nativeQuery.setParameter("compania", "01");
    nativeQuery.setParameter("nume", nume);
    nativeQuery.setParameter("docu", doc);
    List<SalesTRCDTO> result = nativeQuery.getResultList();

    return result;
  }

}
