package com.btw.fe.facturaelectronica.modules.company.repository;

import com.btw.fe.facturaelectronica.modules.company.model.CompanyDTO;
import org.springframework.stereotype.Repository;

@Repository
public interface CompanyRepositoryCustom {

  CompanyDTO findByFechexp(String fecExp);
}
