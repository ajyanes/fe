package com.btw.fe.facturaelectronica.modules.salesTRC.service.impl;

import com.btw.fe.facturaelectronica.modules.salesTRC.model.SalesTRCDTO;
import com.btw.fe.facturaelectronica.modules.salesTRC.repository.SalesTRCRepository;
import com.btw.fe.facturaelectronica.modules.salesTRC.service.SalesTRCService;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SalesTRCServiceImpl implements SalesTRCService {

  @Autowired
  private SalesTRCRepository salesTRCRepository;

  public List<SalesTRCDTO> getValues(String nume, String doc) {
    List<SalesTRCDTO> salesTRCDTOS = salesTRCRepository.findByFechexp(nume, doc);
    for (SalesTRCDTO trcdto : salesTRCDTOS) {
      if (Double.parseDouble(trcdto.getTotalvaluetaxrenta()) == 0.0) {
        salesTRCDTOS = new ArrayList<>();
      }
    }
    return salesTRCDTOS;
  }
}
