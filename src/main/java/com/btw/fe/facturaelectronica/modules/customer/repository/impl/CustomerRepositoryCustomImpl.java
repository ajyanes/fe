package com.btw.fe.facturaelectronica.modules.customer.repository.impl;

import com.btw.fe.facturaelectronica.modules.customer.model.CustomerDTO;
import com.btw.fe.facturaelectronica.modules.customer.repository.CustomerRepositoryCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;

@Repository
@PropertySource("classpath:sentences.xml")
public class CustomerRepositoryCustomImpl implements CustomerRepositoryCustom {

  @Autowired
  private EntityManager em;

  @Autowired
  private Environment env;

  public CustomerDTO findByFechexp(String nume, String doc) {
    String query = env.getProperty("findCustomer.sql");
    Query nativeQuery = em.createNativeQuery(query, CustomerDTO.class);
    nativeQuery.setParameter("compania", "01");
    nativeQuery.setParameter("nume", nume);
    nativeQuery.setParameter("docu", doc);

    CustomerDTO result = (CustomerDTO) nativeQuery.getSingleResult();

    return result;
  }
}
