package com.btw.fe.facturaelectronica.modules.salesTRC.repository;

import com.btw.fe.facturaelectronica.modules.salesTRC.model.SalesTRCDTO;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SalesTRCRepositoryCustom {

  List<SalesTRCDTO> findByFechexp(String nume, String doc);
}
