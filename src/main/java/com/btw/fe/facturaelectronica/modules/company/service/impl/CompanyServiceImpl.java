package com.btw.fe.facturaelectronica.modules.company.service.impl;

import com.btw.fe.facturaelectronica.modules.company.model.CompanyDTO;
import com.btw.fe.facturaelectronica.modules.company.repository.CompanyRepository;
import com.btw.fe.facturaelectronica.modules.company.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CompanyServiceImpl implements CompanyService {

  @Autowired
  private CompanyRepository companyRepository;

  public CompanyDTO getValues(String fecExp) {
    return companyRepository.findByFechexp(fecExp);
  }
}
