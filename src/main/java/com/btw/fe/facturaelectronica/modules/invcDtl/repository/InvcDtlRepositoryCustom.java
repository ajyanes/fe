package com.btw.fe.facturaelectronica.modules.invcDtl.repository;

import com.btw.fe.facturaelectronica.modules.invcDtl.model.InvcDtlDTO;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface InvcDtlRepositoryCustom {

  List<InvcDtlDTO> findByFechexp(String fecExp, String doc);
}
