package com.btw.fe.facturaelectronica.modules.invcDtl.service.impl;

import com.btw.fe.facturaelectronica.modules.invcDtl.model.InvcDtlDTO;
import com.btw.fe.facturaelectronica.modules.invcDtl.model.InvoiceDetail;
import com.btw.fe.facturaelectronica.modules.invcDtl.repository.InvcDtlRepository;
import com.btw.fe.facturaelectronica.modules.invcDtl.service.InvcDtlService;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InvcDtlServiceImpl implements InvcDtlService {

  @Autowired
  private InvcDtlRepository invcDtlRepository;

  public List<InvoiceDetail> getValues(String nume, String doc) {

    return convertDetail(invcDtlRepository.findByFechexp(nume, doc));
  }

  public List<InvoiceDetail> convertDetail(List<InvcDtlDTO> invcDtlDTOS) {
    List<InvoiceDetail> invoiceDetails = new ArrayList<>();
    for (InvcDtlDTO dtlDTO : invcDtlDTOS) {
      InvoiceDetail invoiceDetail = InvoiceDetail.builder()
          .company(dtlDTO.getCompany())
          .invoicenum(dtlDTO.getInvoicenum())
          .invoiceline(dtlDTO.getInvoiceline())
          .partnum(dtlDTO.getPartnum())
          .linedesc(dtlDTO.getLinedesc())
          .partnumpartdescription(dtlDTO.getPartnumpartdescription())
          .sellingshipqty(dtlDTO.getSellingshipqty())
          .salesum(dtlDTO.getSalesum())
          .unitprice(dtlDTO.getUnitprice())
          .docunitprice(dtlDTO.getDocunitprice())
          .docextprice(dtlDTO.getDocextprice())
          .dspdocextprice(dtlDTO.getDspdocextprice())
          .discountpercent(dtlDTO.getDiscountpercent())
          .discount(dtlDTO.getDiscount())
          .docdiscount(dtlDTO.getDocdiscount())
          .dspdoclessdiscount(dtlDTO.getDspdoclessdiscount())
          .dspdoctotalmiscchrg(dtlDTO.getDspdoctotalmiscchrg())
          .currencycode(dtlDTO.getCurrencycode())
          .codinvima(dtlDTO.getCodinvima())
          .poline(dtlDTO.getPoline())
          .character01(dtlDTO.getCharacter01())
          .character02(dtlDTO.getCharacter02())
          .character03(dtlDTO.getCharacter03())
          .character04(dtlDTO.getCharacter04())
          .character05(dtlDTO.getCharacter05())
          .standardItemID(dtlDTO.getStandardItemID())
          .schemeAgencyID(dtlDTO.getSchemeAgencyID())
          .linedesc2(dtlDTO.getLinedesc2())
          .linedesc3(dtlDTO.getLinedesc3())
          .ddvconseregis(dtlDTO.getDdvconseregis())
          .build();
      invoiceDetails.add(invoiceDetail);
    }

    return invoiceDetails;
  }
}
