package com.btw.fe.facturaelectronica.modules.company.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

@Data
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class CompanyDTO {

  @Id
  private String company;
  private String statetaxid;
  private String name;
  private String state;
  private String statenum;
  private String city;
  private String citynum;
  private String identificationtype;
  private String address1;
  private String country;
  private String phonenum;
  private String faxnum;
  private String email;
  private String webpage;
  @Column(name = "regimetype_c")
  private String regimetype_C;
  @Column(name = "fiscalresposability_c")
  private String fiscalresposability_C;
  @Column(name = "operationtype_c")
  private String operationtype_C;
  @Column(name = "companytype_c")
  private String companytype_C;
  @Column(name = "postalzone_c")
  private String postalzone_C;
  @Column(name = "attroperationtype_c")
  private String attroperationtype_C;
  @Transient
  private String industryclassificationcode_C;
}
