package com.btw.fe.facturaelectronica.modules.invcHead.controller;

import com.btw.fe.facturaelectronica.modules.invcHead.model.InvcHeadDTO;
import com.btw.fe.facturaelectronica.modules.invcHead.service.InvcHeadService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.List;

@Slf4j
@RestController
public class InvoicesController {

  @Autowired
  InvcHeadService invcHeadService;

  @GetMapping(value = "/invcHead")
  public ResponseEntity<List<InvcHeadDTO>> getInvoices() {

    String date = LocalDate.now().toString();

    return new ResponseEntity<>(invcHeadService.getValues(date), HttpStatus.ACCEPTED);
  }

}
