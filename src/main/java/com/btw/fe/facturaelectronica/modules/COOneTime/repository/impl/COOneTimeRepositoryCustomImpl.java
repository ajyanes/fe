package com.btw.fe.facturaelectronica.modules.COOneTime.repository.impl;

import com.btw.fe.facturaelectronica.modules.COOneTime.model.COOneTimeDTO;
import com.btw.fe.facturaelectronica.modules.COOneTime.repository.COOneTimeRepositoryCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;

@Repository
@PropertySource("classpath:sentences.xml")
public class COOneTimeRepositoryCustomImpl implements COOneTimeRepositoryCustom {

  @Autowired
  private EntityManager em;

  @Autowired
  private Environment env;

  public COOneTimeDTO findByFechexp(String nume, String doc) {
    String query = env.getProperty("findCOOneTime.sql");
    Query nativeQuery = em.createNativeQuery(query, COOneTimeDTO.class);
    nativeQuery.setParameter("compania", "01");
    nativeQuery.setParameter("nume", nume);
    nativeQuery.setParameter("docu", doc);

    COOneTimeDTO result = (COOneTimeDTO) nativeQuery.getSingleResult();

    return result;
  }
}
