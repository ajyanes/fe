package com.btw.fe.facturaelectronica.modules.invcMisc.service;

import com.btw.fe.facturaelectronica.modules.invcMisc.model.InvcMiscDTO;

import java.util.List;

public interface InvcMiscService {

  List<InvcMiscDTO> getValues(String s, String d);
}
