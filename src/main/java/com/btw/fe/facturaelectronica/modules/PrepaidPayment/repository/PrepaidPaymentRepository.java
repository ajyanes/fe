package com.btw.fe.facturaelectronica.modules.PrepaidPayment.repository;

import com.btw.fe.facturaelectronica.modules.PrepaidPayment.model.PrepaidPaymentDTO;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PrepaidPaymentRepository extends PrepaidPaymentRepositoryCustom,
    CrudRepository<PrepaidPaymentDTO, Integer> {

}
