package com.btw.fe.facturaelectronica.modules.InvcDisc.repository;

import com.btw.fe.facturaelectronica.modules.InvcDisc.model.InvcDiscDTO;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InvcDiscRepository extends InvcDiscRepositoryCustom,
    CrudRepository<InvcDiscDTO, Integer> {

}
