package com.btw.fe.facturaelectronica.modules.AdditionalCustomer.repository;

import com.btw.fe.facturaelectronica.modules.AdditionalCustomer.model.AdditionalCustomerDTO;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AdditionalCustomerRepositoryCustom {

  List<AdditionalCustomerDTO> findByFechexp(String nume, String doc);
}
