package com.btw.fe.facturaelectronica.modules.invcHead.model;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@Data
@Builder
@AllArgsConstructor
@Entity
@Table(name = "CA_ENCDOCVTA")
@NoArgsConstructor
@IdClass(value = InvcHeadDTOPk.class)
public class InvcHeadDTO implements Serializable {

  private String company;
  private String invoicetype;
  @Id
  @Column(name = "Invoicenum")
  private Long invoicenum;
  private String legalnumber;
  private String invoiceref;
  private String invoicerefcufe;
  private String custnum;
  private String contactname;
  private String customername;
  private String invoicedate;
  private String duedate;
  private String dspdocsubtotal;
  private String doctaxamt;
  private String docwhtaxamt;
  private String dspdocinvoiceamt;
  private String invoicecomment;
  private String currencycodecurrencyid;
  private String currencycode;
  private String discount;
  private String tipocons;
  private String resolution1;
  @Column(name = "cmreasoncodec")
  private String cmReasonCode_C;
  @Column(name = "cmreasondescc")
  private String cmReasonDesc_C;
  @Column(name = "dmreasoncodec")
  private String dmReasonCode_C;
  @Column(name = "dmreasondescc")
  private String dmReasonDesc_C;
  @Column(name = "paymentmeansidc")
  private String paymentMeansID_C;

  private String paymentmeansdescription;
  @Column(name = "paymentmeanscodec")
  private String paymentMeansCode_C;
  private String paymentdurationmeasure;
  private LocalDate paymentduedate;
  @Column(name = "calculationratec")
  private String calculationRate_C;
  @Column(name = "datecalculationratec")
  private String datecalculationRate_C;

  @Column(name = "Zipfilebase64c")
  private String zipFileBase64_C;
  private String character01;
  private String character02;
  private String character03;
  private String character04;
  private String character05;
  @Column(name = "Startperioddate")
  private String startPeriodDate;
  @Column(name = "Endperioddate")
  private String endPerioddate;
  private String authorizationnumber;
  private String policynumber;
  private String doctorname;
  private String doctorspecialty;
  private String serviceprovidercode;
  private String mipresnumber;
  private String mipresid;
  private String codoperacionrecaudo;
  private String cantidadacreditar;
  private String conceptorecaudo;
  @Id
  private String edvtipoconsclie;
  private int lecturafe;


}
