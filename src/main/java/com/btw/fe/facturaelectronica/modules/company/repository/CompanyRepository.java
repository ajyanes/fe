package com.btw.fe.facturaelectronica.modules.company.repository;

import com.btw.fe.facturaelectronica.modules.company.model.CompanyDTO;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CompanyRepository extends CompanyRepositoryCustom,
    CrudRepository<CompanyDTO, Integer> {

}
