package com.btw.fe.facturaelectronica.modules.COOneTime.service;

import com.btw.fe.facturaelectronica.modules.COOneTime.model.COOneTimeDTO;

public interface COOneTimeService {

  COOneTimeDTO getValues(String nume, String doc);
}
