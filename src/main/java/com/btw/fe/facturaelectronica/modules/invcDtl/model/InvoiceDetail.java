package com.btw.fe.facturaelectronica.modules.invcDtl.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class InvoiceDetail {

  private String company;
  private String invoicenum;
  private String invoiceline;
  private String partnum;
  private String linedesc;
  private String partnumpartdescription;
  private String sellingshipqty;
  private String salesum;
  private String unitprice;
  private String docunitprice;
  private String docextprice;
  private String dspdocextprice;
  private String discountpercent;
  private String discount;
  private String docdiscount;
  private String dspdoclessdiscount;
  private String dspdoctotalmiscchrg;
  private String currencycode;
  private String codinvima;
  private String poline;
  private String character01;
  private String character02;
  private String character03;
  private String character04;
  private String character05;
  private String standardItemID;
  private String schemeAgencyID;
  private String linedesc2;
  private String linedesc3;
  private Integer ddvconseregis;

}
