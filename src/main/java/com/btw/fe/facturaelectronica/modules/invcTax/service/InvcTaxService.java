package com.btw.fe.facturaelectronica.modules.invcTax.service;

import com.btw.fe.facturaelectronica.modules.invcTax.model.InvcTaxDTO;

import java.util.List;

public interface InvcTaxService {

  List<InvcTaxDTO> getValues(String n, String d);
}
