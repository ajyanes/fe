package com.btw.fe.facturaelectronica.modules.invcMisc.repository.impl;

import com.btw.fe.facturaelectronica.modules.invcMisc.model.InvcMiscDTO;
import com.btw.fe.facturaelectronica.modules.invcMisc.repository.InvcMiscRepositoryCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

@Repository
@PropertySource("classpath:sentences.xml")
public class InvcMiscRepositoryCustomImpl implements InvcMiscRepositoryCustom {

  @Autowired
  EntityManager em;

  @Autowired
  private Environment env;

  public List<InvcMiscDTO> findByEdvfechexp(String nume, String doc) {
    String query = env.getProperty("findInvcMisc.sql");
    Query nativeQuery = em.createNativeQuery(query, InvcMiscDTO.class);
    nativeQuery.setParameter("compania", "01");
    nativeQuery.setParameter("nume", nume);
    nativeQuery.setParameter("docu", doc);
    List<InvcMiscDTO> result = nativeQuery.getResultList();
    return result;
  }
}
