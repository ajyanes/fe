package com.btw.fe.facturaelectronica.modules.invcMisc.model;

import javax.persistence.IdClass;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@Entity
@IdClass(value = InvcMiscPk.class)
public class InvcMiscDTO {

  private String company;
  @Id
  private String invoicetype;
  @Id
  private String invoicenum;
  private Long invoiceline;
  private String misccode;
  private String description;
  private String miscamt;
  private String docmiscamt;
  private String misccodedescription;
  private String percentage;
  private String miscbaseamt;
}
