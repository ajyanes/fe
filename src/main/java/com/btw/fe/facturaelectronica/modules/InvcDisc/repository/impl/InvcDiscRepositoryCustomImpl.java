package com.btw.fe.facturaelectronica.modules.InvcDisc.repository.impl;

import com.btw.fe.facturaelectronica.modules.InvcDisc.model.InvcDiscDTO;
import com.btw.fe.facturaelectronica.modules.InvcDisc.repository.InvcDiscRepositoryCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;

@Repository
@PropertySource("classpath:sentences.xml")
public class InvcDiscRepositoryCustomImpl implements InvcDiscRepositoryCustom {

  @Autowired
  private EntityManager em;

  @Autowired
  private Environment env;

  public InvcDiscDTO findByFechexp(String nume, String doc) {
    String query = env.getProperty("findInvcDisc.sql");
    Query nativeQuery = em.createNativeQuery(query, InvcDiscDTO.class);
    nativeQuery.setParameter("compania", "01");
    nativeQuery.setParameter("nume", nume);
    nativeQuery.setParameter("docu", doc);

    return (InvcDiscDTO) nativeQuery.getSingleResult();

  }
}
