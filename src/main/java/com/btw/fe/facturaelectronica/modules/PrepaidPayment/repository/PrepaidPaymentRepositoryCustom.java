package com.btw.fe.facturaelectronica.modules.PrepaidPayment.repository;

import com.btw.fe.facturaelectronica.modules.PrepaidPayment.model.PrepaidPaymentDTO;
import org.springframework.stereotype.Repository;

@Repository
public interface PrepaidPaymentRepositoryCustom {

  PrepaidPaymentDTO findByFechexp(String doc, String num);
}
