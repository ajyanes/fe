package com.btw.fe.facturaelectronica.modules.InvcDisc.model;

import javax.persistence.IdClass;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@IdClass(value = InvcDiscPk.class)
public class InvcDiscDTO {

  private String company;
  private String invoicetype;
  @Id
  private String invoicecons;
  @Id
  private String invoicenum;
  private String disccode;
  private String description;
  private Long discbaseamount;
  private Long discamt;
  private Long docdiscamt;
  private String disccodedescription;
  private Long percentage;
}
