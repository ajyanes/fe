package com.btw.fe.facturaelectronica.modules.invcDtl.repository;

import com.btw.fe.facturaelectronica.modules.invcDtl.model.InvcDtlDTO;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InvcDtlRepository extends InvcDtlRepositoryCustom,
    CrudRepository<InvcDtlDTO, Integer> {

}
