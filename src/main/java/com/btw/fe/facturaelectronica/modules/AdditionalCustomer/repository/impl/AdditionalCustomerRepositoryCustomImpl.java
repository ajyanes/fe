package com.btw.fe.facturaelectronica.modules.AdditionalCustomer.repository.impl;

import com.btw.fe.facturaelectronica.modules.AdditionalCustomer.model.AdditionalCustomerDTO;
import com.btw.fe.facturaelectronica.modules.AdditionalCustomer.repository.AdditionalCustomerRepositoryCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

@Repository
@PropertySource("classpath:sentences.xml")
public class AdditionalCustomerRepositoryCustomImpl implements AdditionalCustomerRepositoryCustom {

  @Autowired
  private EntityManager em;

  @Autowired
  private Environment env;

  public List<AdditionalCustomerDTO> findByFechexp(String nume, String doc) {
    String query = env.getProperty("findAdditionalCustomer.sql");
    Query nativeQuery = em.createNativeQuery(query, AdditionalCustomerDTO.class);
    nativeQuery.setParameter("compania", "01");
    nativeQuery.setParameter("nume", nume);
    nativeQuery.setParameter("docu", doc);
    List<AdditionalCustomerDTO> result = null;
    result = nativeQuery.getResultList();

    return result;
  }
}
