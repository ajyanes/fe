package com.btw.fe.facturaelectronica.modules.invcHead.service.impl;

import com.btw.fe.facturaelectronica.modules.invcHead.model.InvcHeadDTO;
import com.btw.fe.facturaelectronica.modules.invcHead.repository.InvcHeadRepository;
import com.btw.fe.facturaelectronica.modules.invcHead.repository.impl.InvcHeadRepositoryImpl;
import com.btw.fe.facturaelectronica.modules.invcHead.service.InvcHeadService;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import org.springframework.transaction.annotation.Transactional;

@Service
public class InvcHeadServiceImpl implements InvcHeadService {

  private int counter = 0;
  private int counterDate = 0;

  @Autowired
  private InvcHeadRepositoryImpl invoicesRepository;

  @Autowired
  private InvcHeadRepository invcHeadRepository;

  @Override
  public List<InvcHeadDTO> getValues(String fecExp) {
    return invoicesRepository.findByEdvfechexp(fecExp);
  }

  @Override
  public List<InvcHeadDTO> getValuesByState(String state) {
    return invoicesRepository.findByState(state);
  }

  @Override
  public List<InvcHeadDTO> getValuesByDateRange(String startDate, String endDate) {
    return invoicesRepository.findByDateRange(startDate, endDate);
  }

  @Override
  public List<InvcHeadDTO> getValuesByFilter(String startDate, String endDate, String docType,
      String invoice, String legalNumber) {
    counter=0;
    StringBuilder query = new StringBuilder();
    String conditionalDate = validateRange(startDate, endDate);

      query.append(conditionalDate);

    if (!docType.equals("''")) {
      query.append(" and edvclasdocuclie = " + docType);
      counter++;
    }

    if (!invoice.equals("''")) {
      query.append(" and edvnumedocuclie = " + invoice);
      counter++;
    }

    if (!legalNumber.equals("''")) {
      query.append(" and edvtipoconsclie+'-'+edvnumedocuclie = " + legalNumber);
      counter++;
    }

    if(counter==0) {
      query.append(getDocuments());
    }

    return invoicesRepository.findByApplyFilters(query);
  }

  @Override
  public List<Date> getFechaInicio(String doc) {
    return invoicesRepository.fechaInicio(doc);
  }

  @Override
  public List<Date> getFechaFin(String doc) {
    return invoicesRepository.fechaFin(doc);
  }

  @Override
  public List<BigDecimal> getCuotaModerado(String doc) {
    return invoicesRepository.cuotaModerado(doc);
  }

  @Override
  public List<BigDecimal> getCopago(String doc) {
    return invoicesRepository.copago(doc);
  }

  @Override
  public List<BigDecimal> getCuotaRecuperacion(String doc) {
    return invoicesRepository.cuotaRecuperacion(doc);
  }

  @Override
  public List<String> getPagosCompartidos(String doc) {
    return invoicesRepository.pagosCompartidos(doc);
  }

  @Override
  public List<String> getContrato(String doc) {
    return invoicesRepository.contrato(doc);
  }

  public StringBuilder getDocuments(){
    StringBuilder addQuery = new StringBuilder(" and edvtipoconsclie in (");
    List<String> strings = invoicesRepository.getDocuments();
    int tam = strings.size();
    int it = 1;
    if(tam > 0) {
      for (String s : strings) {
        addQuery.append("'"+s+"'");
        if (it < tam) {
          addQuery.append(",");
        }
        it++;
      }
      addQuery.append(")");

      return addQuery;
    }else{
      return new StringBuilder();
    }
  }

  @Transactional
  @Override
  public void updateLecturaFe(List<InvcHeadDTO> invcHeadDTOS) {
    try {
      invoicesRepository.updateMarkInvoice(invcHeadDTOS);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public String validateRange(String startDate, String endDate) {
    String conditional = "";
    if (!startDate.equals("''")) {
     conditional = " and convert(varchar(10),edvfechexpe,103) = " + startDate;
     counterDate++;
    }

    if (!endDate.equals(null) && !endDate.isEmpty() && !endDate.equals("''")) {
      if(counterDate>0) {
        conditional =
            " and convert(varchar(10),edvfechexpe,103) between " + startDate + " and " + endDate;
      }else{
        counterDate = 100;
        conditional = "La fecha inicial no puede estar vacia";
      }
    }

    return conditional;
  }

}
