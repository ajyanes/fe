package com.btw.fe.facturaelectronica.modules.invcTax.repository.impl;

import com.btw.fe.facturaelectronica.modules.invcTax.model.InvcTaxDTO;
import com.btw.fe.facturaelectronica.modules.invcTax.repository.InvcTaxRepositoryCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

@Repository
@PropertySource("classpath:sentences.xml")
public class InvcTaxRepositoryCustomImpl implements InvcTaxRepositoryCustom {

  @Autowired
  EntityManager em;

  @Autowired
  private Environment env;

  public List<InvcTaxDTO> findByEdvfechexp(String nume, String doc) {
    String query = env.getProperty("findInvcTax.sql");
    Query nativeQuery = em.createNativeQuery(query, InvcTaxDTO.class);
    nativeQuery.setParameter("compania", "01");
    nativeQuery.setParameter("nume", nume);
    nativeQuery.setParameter("docu", doc);
    List<InvcTaxDTO> result = nativeQuery.getResultList();
    return result;
  }
}
