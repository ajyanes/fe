package com.btw.fe.facturaelectronica.modules.invcMisc.service.impl;

import com.btw.fe.facturaelectronica.modules.invcMisc.model.InvcMiscDTO;
import com.btw.fe.facturaelectronica.modules.invcMisc.repository.InvcMiscRepositoryCustom;
import com.btw.fe.facturaelectronica.modules.invcMisc.service.InvcMiscService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InvcMiscServiceImpl implements InvcMiscService {

  @Autowired
  private InvcMiscRepositoryCustom custom;

  public List<InvcMiscDTO> getValues(String n, String d) {
    return custom.findByEdvfechexp(n, d);
  }
}
