package com.btw.fe.facturaelectronica.modules.invcHead.repository;

import com.btw.fe.facturaelectronica.modules.invcHead.model.InvcHeadDTO;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InvcHeadRepository extends CrudRepository<InvcHeadDTO, Long> {

}
