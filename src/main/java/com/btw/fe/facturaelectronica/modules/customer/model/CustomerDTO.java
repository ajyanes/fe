package com.btw.fe.facturaelectronica.modules.customer.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

@Data
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class CustomerDTO {

  private String company;
  private String custid;
  @Id
  private String custnum;
  private String resaleid;
  private String name;
  private String address1;
  private String emailaddress;
  private String phonenum;
  private String currencycode;
  private String address2;
  private String address3;
  private String country;
  private String codpostal;
  private String faxnum;
  private String state;
  private String statenum;
  private String city;
  private String citynum;
  @Column(name = "regimetype_c")
  private String regimetype_C;
  @Column(name = "fiscalresposability_C")
  private String fiscalresposability_C;
  @Transient
  private String patientname;
  @Transient
  private String patientfamilyname1;
  @Transient
  private String patientfamilyname2;
  @Transient
  private String patientfirstname;
  @Transient
  private String patientfirstname2;
  @Transient
  private String idpatient;
  @Transient
  private String tipodocpatient;
  @Transient
  private String patientAddress;
  @Transient
  private String patientcity;
  @Transient
  private String patientcityNum;
  @Transient
  private String patientemail;
  @Transient
  private String patientcountry;
  @Transient
  private String patientnumhistory;
  @Transient
  private String patientphonenum;
  @Transient
  private String patientbirthday;
  @Transient
  private String patientage;
  @Transient
  private String patientregime;
  @Transient
  private String patientpaymentMode;
  @Transient
  private String patientcoverage;
  @Transient
  private String admissionDate;
  @Transient
  private String dischargedate;
}
