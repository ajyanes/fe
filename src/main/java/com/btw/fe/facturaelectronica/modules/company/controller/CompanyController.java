package com.btw.fe.facturaelectronica.modules.company.controller;

import com.btw.fe.facturaelectronica.modules.company.model.CompanyDTO;
import com.btw.fe.facturaelectronica.modules.company.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CompanyController {

  @Autowired
  CompanyService service;

  @GetMapping("/company")
  public ResponseEntity<CompanyDTO> getValues() {
    return new ResponseEntity<>(service.getValues(""), HttpStatus.ACCEPTED);
  }
}
