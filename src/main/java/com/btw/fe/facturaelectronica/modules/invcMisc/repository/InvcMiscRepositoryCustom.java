package com.btw.fe.facturaelectronica.modules.invcMisc.repository;

import com.btw.fe.facturaelectronica.modules.invcMisc.model.InvcMiscDTO;

import java.util.List;

public interface InvcMiscRepositoryCustom {

  List<InvcMiscDTO> findByEdvfechexp(String nume, String doc);
}
