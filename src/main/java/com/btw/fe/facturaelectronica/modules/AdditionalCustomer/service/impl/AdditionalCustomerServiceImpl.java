package com.btw.fe.facturaelectronica.modules.AdditionalCustomer.service.impl;

import com.btw.fe.facturaelectronica.modules.AdditionalCustomer.model.AdditionalCustomerDTO;
import com.btw.fe.facturaelectronica.modules.AdditionalCustomer.repository.AdditionalCustomerRepository;
import com.btw.fe.facturaelectronica.modules.AdditionalCustomer.service.AdditionalCustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdditionalCustomerServiceImpl implements AdditionalCustomerService {

  @Autowired
  private AdditionalCustomerRepository additionalCustomerRepository;

  public List<AdditionalCustomerDTO> getValues(String nume, String doc) {
    return additionalCustomerRepository.findByFechexp(nume, doc);
  }
}
