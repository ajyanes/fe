package com.btw.fe.facturaelectronica.modules.COOneTime.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import javax.persistence.Column;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class COOneTimeDTO {

  private String company;
  private String identificationtype;
  @Id
  @Column(name = "edvcliente")
  @JsonProperty("COOneTimeID")
  private String COOneTimeID;
  private String companyname;
  private String countrycode;

}

