package com.btw.fe.facturaelectronica.modules.customer.service.impl;

import com.btw.fe.facturaelectronica.modules.customer.model.CustomerDTO;
import com.btw.fe.facturaelectronica.modules.customer.repository.CustomerRepository;
import com.btw.fe.facturaelectronica.modules.customer.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomerServiceImpl implements CustomerService {

  @Autowired
  private CustomerRepository customerRepository;

  public CustomerDTO getValues(String nume, String doc) {
    return customerRepository.findByFechexp(nume, doc);
  }
}
