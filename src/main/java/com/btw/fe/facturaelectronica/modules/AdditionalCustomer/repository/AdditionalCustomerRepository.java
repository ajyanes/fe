package com.btw.fe.facturaelectronica.modules.AdditionalCustomer.repository;

import com.btw.fe.facturaelectronica.modules.AdditionalCustomer.model.AdditionalCustomerDTO;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AdditionalCustomerRepository extends AdditionalCustomerRepositoryCustom,
    CrudRepository<AdditionalCustomerDTO, Integer> {

}
