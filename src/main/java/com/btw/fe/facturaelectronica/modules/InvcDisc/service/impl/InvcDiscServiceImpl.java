package com.btw.fe.facturaelectronica.modules.InvcDisc.service.impl;

import com.btw.fe.facturaelectronica.modules.InvcDisc.model.InvcDiscDTO;
import com.btw.fe.facturaelectronica.modules.InvcDisc.repository.InvcDiscRepository;
import com.btw.fe.facturaelectronica.modules.InvcDisc.service.InvcDiscService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class InvcDiscServiceImpl implements InvcDiscService {

  @Autowired
  private InvcDiscRepository invcDiscRepository;

  public InvcDiscDTO getValues(String nume, String doc) {
    return invcDiscRepository.findByFechexp(nume, doc);
  }
}
