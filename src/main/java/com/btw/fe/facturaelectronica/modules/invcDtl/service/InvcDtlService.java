package com.btw.fe.facturaelectronica.modules.invcDtl.service;

import com.btw.fe.facturaelectronica.modules.invcDtl.model.InvoiceDetail;
import java.util.List;

public interface InvcDtlService {

  List<InvoiceDetail> getValues(String nume, String doc);


}
