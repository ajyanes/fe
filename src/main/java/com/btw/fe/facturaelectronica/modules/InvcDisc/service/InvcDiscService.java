package com.btw.fe.facturaelectronica.modules.InvcDisc.service;

import com.btw.fe.facturaelectronica.modules.InvcDisc.model.InvcDiscDTO;

public interface InvcDiscService {

  InvcDiscDTO getValues(String nume, String doc);
}
