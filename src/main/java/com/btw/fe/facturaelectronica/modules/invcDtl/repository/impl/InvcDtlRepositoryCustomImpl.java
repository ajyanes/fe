package com.btw.fe.facturaelectronica.modules.invcDtl.repository.impl;

import com.btw.fe.facturaelectronica.modules.invcDtl.model.InvcDtlDTO;
import com.btw.fe.facturaelectronica.modules.invcDtl.repository.InvcDtlRepositoryCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

@Repository
@PropertySource("classpath:sentences.xml")
public class InvcDtlRepositoryCustomImpl implements InvcDtlRepositoryCustom {

  @Autowired
  private EntityManager em;

  @Autowired
  private Environment env;

  public List<InvcDtlDTO> findByFechexp(String numedocu, String doc) {
    String query = env.getProperty("findInvcDtl.sql");
    Query nativeQuery = em.createNativeQuery(query, InvcDtlDTO.class);
    nativeQuery.setParameter("compania", "01");
    nativeQuery.setParameter("numedocu", numedocu);
    nativeQuery.setParameter("docu", doc);

    List<InvcDtlDTO> result = nativeQuery.getResultList();
    return result;
  }
}
