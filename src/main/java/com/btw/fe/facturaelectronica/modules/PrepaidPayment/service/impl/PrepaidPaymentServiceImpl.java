package com.btw.fe.facturaelectronica.modules.PrepaidPayment.service.impl;

import com.btw.fe.facturaelectronica.modules.PrepaidPayment.model.PrepaidPaymentDTO;
import com.btw.fe.facturaelectronica.modules.PrepaidPayment.repository.PrepaidPaymentRepository;
import com.btw.fe.facturaelectronica.modules.PrepaidPayment.service.PrepaidPaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PrepaidPaymentServiceImpl implements PrepaidPaymentService {

  @Autowired
  private PrepaidPaymentRepository prepaidPaymentRepository;

  public PrepaidPaymentDTO getValues(String doc, String num) {
    return prepaidPaymentRepository.findByFechexp(doc, num);
  }
}
