package com.btw.fe.facturaelectronica.modules.InvcDisc.repository;

import com.btw.fe.facturaelectronica.modules.InvcDisc.model.InvcDiscDTO;
import org.springframework.stereotype.Repository;

@Repository
public interface InvcDiscRepositoryCustom {

  InvcDiscDTO findByFechexp(String nume, String doc);
}
