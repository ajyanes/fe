package com.btw.fe.facturaelectronica.modules.AdditionalCustomer.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class AdditionalCustomerDTO {

  private String patientname;
  private String patientfamilyname1;
  private String patientfamilyname2;
  private String patientfirstname;
  private String patientfirstname2;
  @Id
  private String idpatient;
  private String tipodocpatient;
  private String patientaddress;
  private String patientcity;
  private String patientcitynum;
  private String patientemail;
  private String patientcountry;
  private String patientnumhistory;
  private String patientphonenum;
  private String patientbirthday;
  private String patientage;
  private String patientregime;
  private String patientpaymentmode;
  private String patientcoverage;
  private String admissiondate;
  private String dischargedate;
  private String authorizationnumber;
  private String policynumber;
  private String moderatingfeeamt;
  private String copaymentamt;
  private String retrievalfeeamt;
  private String sharedpaytmentamt;
  private String serviceprovidercode;
  private String mipresnumber;
  private String mipresid;
  private String contractnumber;
}
