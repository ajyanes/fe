package com.btw.fe.facturaelectronica.modules.PrepaidPayment.repository.impl;

import com.btw.fe.facturaelectronica.modules.PrepaidPayment.model.PrepaidPaymentDTO;
import com.btw.fe.facturaelectronica.modules.PrepaidPayment.repository.PrepaidPaymentRepositoryCustom;
import java.util.ArrayList;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

@Transactional
@Repository
@PropertySource("classpath:sentences.xml")
public class PrepaidPaymentRepositoryCustomImpl implements PrepaidPaymentRepositoryCustom {

  @Autowired
  private EntityManager em;

  @Autowired
  private Environment env;

  public PrepaidPaymentDTO findByFechexp(String doc, String num) {
    String query = env.getProperty("findPrepaidPayment.sql");
    Query nativeQuery = em.createNativeQuery(query, PrepaidPaymentDTO.class);
    nativeQuery.setParameter("compania", "01");
    nativeQuery.setParameter("doc", doc);
    nativeQuery.setParameter("num", num);
    nativeQuery.setHint("org.hibernate.fetchSize",500);

    List<PrepaidPaymentDTO> result = new ArrayList<>();

    result = nativeQuery.getResultList();

    return result.size() == 0 ? new PrepaidPaymentDTO() : result.get(0);
  }
}
