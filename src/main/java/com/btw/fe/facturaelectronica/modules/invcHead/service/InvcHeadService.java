package com.btw.fe.facturaelectronica.modules.invcHead.service;

import com.btw.fe.facturaelectronica.modules.invcHead.model.InvcHeadDTO;


import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;

public interface InvcHeadService {

  List<InvcHeadDTO> getValues(String fecExp);

  List<InvcHeadDTO> getValuesByState(String state);

  List<InvcHeadDTO> getValuesByDateRange(String startDate, String endDate);

  List<InvcHeadDTO> getValuesByFilter(String startDate, String endDate, String docType,
      String invoice, String legalNumber);

  List<Date> getFechaInicio(String doc);

  List<Date> getFechaFin(String doc);

  List<BigDecimal> getCuotaModerado(String doc);

  List<BigDecimal> getCopago(String doc);

  List<BigDecimal> getCuotaRecuperacion(String doc);

  List<String> getPagosCompartidos(String doc);

  List<String> getContrato(String doc);

  String validateRange(String startDate, String endDate);

  void updateLecturaFe(List<InvcHeadDTO> invcHeadDTOS);

}
