package com.btw.fe.facturaelectronica.modules.customer.repository;

import com.btw.fe.facturaelectronica.modules.customer.model.CustomerDTO;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepository extends CustomerRepositoryCustom,
    CrudRepository<CustomerDTO, Integer> {

}
