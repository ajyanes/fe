package com.btw.fe.facturaelectronica.modules.invcTax.repository;

import com.btw.fe.facturaelectronica.modules.invcTax.model.InvcTaxDTO;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InvcTaxRepository extends CrudRepository<InvcTaxDTO, String> {

}
