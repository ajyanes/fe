package com.btw.fe.facturaelectronica.modules.COOneTime.repository;

import com.btw.fe.facturaelectronica.modules.COOneTime.model.COOneTimeDTO;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface COOneTimeRepository extends COOneTimeRepositoryCustom,
    CrudRepository<COOneTimeDTO, Integer> {

}
