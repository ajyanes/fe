package com.btw.fe.facturaelectronica.modules.invcTax.service.impl;

import com.btw.fe.facturaelectronica.modules.invcTax.model.InvcTaxDTO;
import com.btw.fe.facturaelectronica.modules.invcTax.repository.InvcTaxRepositoryCustom;
import com.btw.fe.facturaelectronica.modules.invcTax.service.InvcTaxService;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InvcTaxServiceImpl implements InvcTaxService {

  @Autowired
  private InvcTaxRepositoryCustom custom;

  public List<InvcTaxDTO> getValues(String s, String d) {
    List<InvcTaxDTO> invcTaxDTOS = custom.findByEdvfechexp(s, d);

    for (InvcTaxDTO taxDTO : invcTaxDTOS) {
      if (taxDTO.getInvoiceline().equals("0")) {
        invcTaxDTOS = new ArrayList<>();
      }
    }
    return invcTaxDTOS;
  }
}
