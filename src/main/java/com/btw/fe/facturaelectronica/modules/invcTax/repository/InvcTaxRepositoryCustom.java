package com.btw.fe.facturaelectronica.modules.invcTax.repository;

import com.btw.fe.facturaelectronica.modules.invcTax.model.InvcTaxDTO;

import java.util.List;

public interface InvcTaxRepositoryCustom {

  List<InvcTaxDTO> findByEdvfechexp(String nume, String docu);
}
