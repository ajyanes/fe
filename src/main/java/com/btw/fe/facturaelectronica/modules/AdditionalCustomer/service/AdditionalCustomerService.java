package com.btw.fe.facturaelectronica.modules.AdditionalCustomer.service;

import com.btw.fe.facturaelectronica.modules.AdditionalCustomer.model.AdditionalCustomerDTO;

import java.util.List;

public interface AdditionalCustomerService {

  List<AdditionalCustomerDTO> getValues(String nume, String doc);
}
