package com.btw.fe.facturaelectronica.modules.COOneTime.service.impl;

import com.btw.fe.facturaelectronica.modules.COOneTime.model.COOneTimeDTO;
import com.btw.fe.facturaelectronica.modules.COOneTime.service.COOneTimeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class COOneTimeServiceImpl implements COOneTimeService {

  @Autowired
  private com.btw.fe.facturaelectronica.modules.COOneTime.repository.COOneTimeRepository COOneTimeRepository;

  public COOneTimeDTO getValues(String nume, String doc) {
    return COOneTimeRepository.findByFechexp(nume, doc);
  }
}
