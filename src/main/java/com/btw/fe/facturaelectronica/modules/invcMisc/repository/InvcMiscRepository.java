package com.btw.fe.facturaelectronica.modules.invcMisc.repository;

import com.btw.fe.facturaelectronica.modules.invcMisc.model.InvcMiscDTO;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InvcMiscRepository extends CrudRepository<InvcMiscDTO, String> {

}
