package com.btw.fe.facturaelectronica.modules.company.repository.impl;

import com.btw.fe.facturaelectronica.modules.company.model.CompanyDTO;
import com.btw.fe.facturaelectronica.modules.company.repository.CompanyRepositoryCustom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Repository;
import javax.persistence.EntityManager;
import javax.persistence.Query;

@Repository
@PropertySource("classpath:sentences.xml")
public class CompanyRepositoryCustomImpl implements CompanyRepositoryCustom {

  @Autowired
  private EntityManager em;

  @Autowired
  private Environment env;

  public CompanyDTO findByFechexp(String fecExp) {
    String query = env.getProperty("findCompany.sql");
    Query nativeQuery = em.createNativeQuery(query, CompanyDTO.class);
    nativeQuery.setParameter("compania", "01");
    CompanyDTO result = (CompanyDTO) nativeQuery.getSingleResult();
    return result;
  }
}
