package com.btw.fe.facturaelectronica.modules.customer.repository;

import com.btw.fe.facturaelectronica.modules.customer.model.CustomerDTO;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepositoryCustom {

  CustomerDTO findByFechexp(String nume, String doc);
}
