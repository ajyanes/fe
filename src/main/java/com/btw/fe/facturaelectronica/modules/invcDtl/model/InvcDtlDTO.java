package com.btw.fe.facturaelectronica.modules.invcDtl.model;

import java.io.Serializable;
import javax.persistence.IdClass;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

@Data
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@IdClass(value = InvcDtlDTOPk.class)
public class InvcDtlDTO implements Serializable {

  private String company;
  @Id
  private String invoicenum;
  @Id
  private String invoicetype;
  @Id
  private String invoiceline;
  private String partnum;
  private String linedesc;
  private String partnumpartdescription;
  private String sellingshipqty;
  private String salesum;
  private String unitprice;
  private String docunitprice;
  private String docextprice;
  private String dspdocextprice;
  private String discountpercent;
  private String discount;
  private String docdiscount;
  private String dspdoclessdiscount;
  private String dspdoctotalmiscchrg;
  private String currencycode;
  @Transient
  private String codinvima;
  @Transient
  private String poline;
  @Transient
  private String character01;
  @Transient
  private String character02;
  @Transient
  private String character03;
  @Transient
  private String character04;
  @Transient
  private String character05;
  @Transient
  private String standardItemID;
  @Transient
  private String schemeAgencyID;
  @Transient
  private String linedesc2;
  @Transient
  private String linedesc3;
  @Transient
  private Integer ddvconseregis;
}
