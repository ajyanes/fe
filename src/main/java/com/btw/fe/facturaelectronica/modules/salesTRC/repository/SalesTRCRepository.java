package com.btw.fe.facturaelectronica.modules.salesTRC.repository;

import com.btw.fe.facturaelectronica.modules.salesTRC.model.SalesTRCDTO;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SalesTRCRepository extends SalesTRCRepositoryCustom,
    CrudRepository<SalesTRCDTO, Integer> {

}
