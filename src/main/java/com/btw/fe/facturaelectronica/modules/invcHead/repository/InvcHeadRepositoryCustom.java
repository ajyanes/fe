package com.btw.fe.facturaelectronica.modules.invcHead.repository;

import com.btw.fe.facturaelectronica.modules.invcHead.model.InvcHeadDTO;


import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;

public interface InvcHeadRepositoryCustom {

  List<InvcHeadDTO> findByEdvfechexp(String fecExp);

  List<InvcHeadDTO> findByState(String state);

  List<InvcHeadDTO> findByDateRange(String startDate, String endDate);

  List<InvcHeadDTO> findByApplyFilters(StringBuilder query);

  List<String> getDocuments();

  void updateMarkInvoice(List<InvcHeadDTO> invcHeadDTOS);

  List<Date> fechaInicio(String doc);

  List<Date> fechaFin(String doc);

  List<BigDecimal> cuotaModerado(String doc);

  List<BigDecimal> copago(String doc);

  List<BigDecimal> cuotaRecuperacion(String doc);

  List<String> pagosCompartidos(String doc);

  List<String> contrato(String doc);

}
