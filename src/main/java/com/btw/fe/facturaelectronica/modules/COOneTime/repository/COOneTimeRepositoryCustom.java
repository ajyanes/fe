package com.btw.fe.facturaelectronica.modules.COOneTime.repository;

import com.btw.fe.facturaelectronica.modules.COOneTime.model.COOneTimeDTO;
import org.springframework.stereotype.Repository;

@Repository
public interface COOneTimeRepositoryCustom {

  COOneTimeDTO findByFechexp(String nume, String doc);
}
