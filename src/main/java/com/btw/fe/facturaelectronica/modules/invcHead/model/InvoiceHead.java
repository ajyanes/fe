package com.btw.fe.facturaelectronica.modules.invcHead.model;

import java.math.BigDecimal;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class InvoiceHead {

  private String company;
  private String invoicetype;
  private Long invoicenum;
  private String legalnumber;
  private String invoiceref;
  private String invoicerefcufe;
  private String custnum;
  private String contactname;
  private String customername;
  private String invoicedate;
  private String duedate;
  private String dspdocsubtotal;
  private String doctaxamt;
  private String docwhtaxamt;
  private String dspdocinvoiceamt;
  private String invoicecomment;
  private String currencycodecurrencyid;
  private String currencycode;
  private String discount;
  private String resolution1;
  private String cmReasonCode_C;
  private String cmReasonDesc_C;
  private String dmReasonCode_C;
  private String dmReasonDesc_C;
  private String paymentMeansID_C;
  private String paymentmeansdescription;
  private String paymentMeansCode_C;
  private String paymentdurationmeasure;
  private LocalDate paymentduedate;
  private String calculationRate_C;
  private String datecalculationRate_C;
  private String zipFileBase64_C;
  private String character01;
  private String character02;
  private String character03;
  private String character04;
  private String character05;
  private String startPeriodDate;
  private String endPeriodDate;
  private String authorizationnumber;
  private String policynumber;
  private String doctorname;
  private String doctorspecialty;
  private List<BigDecimal> moderatingfeeamt;
  private List<BigDecimal> copaymentamt;
  private List<BigDecimal> retrievalfeeamt;
  private List<String> sharedpaymentamt;
  private String serviceprovidercode;
  private List<String> contractnumber;
  private String mipresnumber;
  private String mipresid;
  private String codoperacionrecaudo;
  private String cantidadacreditar;
  private String conceptorecaudo;
  private int lecturafe;

}
