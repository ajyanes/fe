package com.btw.fe.facturaelectronica.modules.salesTRC.service;

import com.btw.fe.facturaelectronica.modules.salesTRC.model.SalesTRCDTO;

import java.util.List;

public interface SalesTRCService {

  List<SalesTRCDTO> getValues(String nume, String doc);
}
