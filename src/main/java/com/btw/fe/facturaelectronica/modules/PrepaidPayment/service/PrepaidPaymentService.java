package com.btw.fe.facturaelectronica.modules.PrepaidPayment.service;

import com.btw.fe.facturaelectronica.modules.PrepaidPayment.model.PrepaidPaymentDTO;

public interface PrepaidPaymentService {

  PrepaidPaymentDTO getValues(String doc, String num);
}
