package com.btw.fe.facturaelectronica.modules.invcTax.model;

import javax.persistence.Column;
import javax.persistence.IdClass;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@Entity
@IdClass(value = InvcTaxPk.class)
public class InvcTaxDTO {

  private String company;
  @Id
  private String invoicetype;
  @Id
  private String invoicenum;
  private String invoiceline;
  private String currencycode;

  private String ratecode;
  private String doctaxableamt;
  private String taxamt;
  private String doctaxamt;
  @Column(name = "valuepercent")
  private String percent;
  @Column(name = "Withholdingtaxc")
  private String withholdingtax_C;
  @Transient
  @Column(name = "baseunitmeasurec")
  private String baseunitmeasure_c;
  @Transient
  @Column(name = "perunitamountc")
  private String perunitamount_c;
}
