package com.btw.fe.facturaelectronica.modules.customer.service;

import com.btw.fe.facturaelectronica.modules.customer.model.CustomerDTO;

public interface CustomerService {

  CustomerDTO getValues(String nume, String doc);
}
