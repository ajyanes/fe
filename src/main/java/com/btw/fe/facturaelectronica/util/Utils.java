package com.btw.fe.facturaelectronica.util;

public class Utils {

  public Utils() {

  }

  public static int getPeriod(String fecha) {
    String period = ""; // dd/mm/yyyy

    period = fecha.substring(3, 5);

    return Integer.parseInt(period);
  }

  public static int getYear(String fecha) {
    String year = ""; // dd/mm/yyyy

    year = fecha.substring(6, 10);

    return Integer.parseInt(year);

  }

}
