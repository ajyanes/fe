package com.btw.fe.additionalInfo.repository;

import com.btw.fe.additionalInfo.domain.Additional;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface AdditionalRepository extends JpaRepository<Additional, Long> {

  @Query(value = "select edvcliente from ca_encdocvta"
      + " where edvtipoconsclie = :tipocons"
      + " and edvnumedocuclie = :cons",nativeQuery = true)
  String findByTipoconsAndCons(String tipocons, String cons);

  List<Additional> findByNumerofactura(String bill);

  Long deleteByEdvsecuenciaAndNumerofactura(Long id, String bill);

  Long removeByEdvsecuenciaAndNumerofactura(Long id, String bill);
}
