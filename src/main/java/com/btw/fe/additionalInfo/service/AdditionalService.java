package com.btw.fe.additionalInfo.service;

import com.btw.fe.additionalInfo.domain.AdditionalDto;
import com.btw.fe.additionalInfo.domain.AdditionalReponse;
import com.btw.fe.additionalInfo.domain.AdditionalUpdateDto;
import java.util.List;

public interface AdditionalService {

  AdditionalReponse saveInfo(AdditionalReponse additionalReponse);

  AdditionalReponse get(String tipocons, String cons);

  void delete(String id, String bill);

  AdditionalReponse update(AdditionalReponse additionalReponse);
}
