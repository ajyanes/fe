package com.btw.fe.additionalInfo.service.impl;

import com.btw.fe.Util.MessageResource;
import com.btw.fe.additionalInfo.domain.Additional;
import com.btw.fe.additionalInfo.domain.AdditionalDto;
import com.btw.fe.additionalInfo.domain.AdditionalReponse;
import com.btw.fe.additionalInfo.domain.AdditionalUpdateDto;
import com.btw.fe.additionalInfo.mappers.AdditionalMapper;
import com.btw.fe.additionalInfo.repository.AdditionalRepository;
import com.btw.fe.additionalInfo.service.AdditionalService;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.transaction.Transactional;
import lombok.var;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AdditionalServiceImpl implements AdditionalService {

  @Autowired
  private AdditionalRepository additionalRepository;

  AdditionalMapper additionalMapper = Mappers.getMapper(AdditionalMapper.class);

  @Override
  public AdditionalReponse saveInfo(AdditionalReponse additionalReponse) {
    AdditionalReponse additionalDtos = new AdditionalReponse();

    for(AdditionalDto additionalDto : additionalReponse.getAdditionalDtos()) {
      if (additionalDto.getFechainicio().isAfter(additionalDto.getFechafinal())) {
        throw new RuntimeException(MessageResource.INITIAL_DATE_AFTER_FINAL_DATE.getName());
      }
    }

    List<Additional> additionals = additionalMapper.fromListDto(additionalReponse.getAdditionalDtos());

     List<Additional> additional = additionalRepository.saveAll(additionals);

    additionalDtos.setAdditionalDtos(additionalMapper.fromListEntity(additional));

     return additionalDtos;
  }

  @Override
  public AdditionalReponse get(String tipocons, String cons) {
    AdditionalReponse additionalReponse = new AdditionalReponse();
    String customer = additionalRepository.findByTipoconsAndCons(tipocons, cons);

    if(Objects.isNull(customer)){
      throw new RuntimeException(MessageResource.RECORD_NOT_EXISTS.getName());
    }

    String bill = tipocons+cons;

    List<Additional> additionals = additionalRepository.findByNumerofactura(bill);

    if(additionals.isEmpty()){
      additionalReponse.setAdditionalDtos(new ArrayList<>());
      additionalReponse.setCustomer(customer);
      return additionalReponse;
    }

    additionalReponse.setAdditionalDtos(additionalMapper.fromListEntity(additionals));
    additionalReponse.setCustomer(customer);

    return additionalReponse;
  }

  @Transactional
  @Override
  public void delete(String id,String bill) {

  additionalRepository.deleteByEdvsecuenciaAndNumerofactura(Long.parseLong(id), bill);

  }

  @Override
  public AdditionalReponse update(AdditionalReponse additionalUpdateDto) {
    AdditionalReponse additionalReponse = new AdditionalReponse();

    for(AdditionalDto additionalDto: additionalUpdateDto.getAdditionalDtos()){
      if(additionalDto.getFechainicio().isAfter(additionalDto.getFechafinal())){
        throw new RuntimeException(MessageResource.INITIAL_DATE_AFTER_FINAL_DATE.getName());
      }
    }

    var additionalObject = additionalMapper.fromListDto(additionalUpdateDto.getAdditionalDtos());

    List<Additional> additionals = additionalRepository.saveAll(additionalObject);

    additionalReponse.setAdditionalDtos(additionalMapper.fromListEntity(additionals));

    return additionalReponse;
  }


}
