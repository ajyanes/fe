package com.btw.fe.additionalInfo.mappers;

import com.btw.fe.additionalInfo.domain.Additional;
import com.btw.fe.additionalInfo.domain.AdditionalDto;
import com.btw.fe.additionalInfo.domain.AdditionalUpdateDto;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants;
import org.mapstruct.ReportingPolicy;

@Mapper(
    componentModel = MappingConstants.ComponentModel.SPRING,
    unmappedTargetPolicy = ReportingPolicy.IGNORE
)
public interface AdditionalMapper {

  AdditionalDto fromEntity(Additional additional);


  @Mapping(target = "edvtipoconsclie", source = "additionalDto.edvtipoconsclie")
  @Mapping(target = "edvnumedocuclie", source = "additionalDto.edvnumedocuclie")
  Additional fromDto(AdditionalDto additionalDto);

  @Mapping(target = "numerofactura", source = "additionalUpdateDto.numerofactura")
  @Mapping(target = "codigoprestador", source = "additionalUpdateDto.codigoprestador")
  @Mapping(target = "modalidadcontrato", source = "additionalUpdateDto.modalidadcontrato")
  @Mapping(target = "cobertura", source = "additionalUpdateDto.cobertura")
  @Mapping(target = "numerocontrato", source = "additionalUpdateDto.numerocontrato")
  @Mapping(target = "numeropoliza", source = "additionalUpdateDto.numeropoliza")
  @Mapping(target = "copago", source = "additionalUpdateDto.copago")
  @Mapping(target = "cuotamoderado", source = "additionalUpdateDto.cuotamoderado")
  @Mapping(target = "cuotarecuperacion", source = "additionalUpdateDto.cuotarecuperacion")
  @Mapping(target = "pagoscompartidos", source = "additionalUpdateDto.pagoscompartidos")
  @Mapping(target = "fechainicio", source = "additionalUpdateDto.fechainicio")
  @Mapping(target = "fechafinal", source = "additionalUpdateDto.fechafinal")
  Additional fromUpdate(Additional additional, AdditionalUpdateDto additionalUpdateDto);


  List<Additional> fromListDto(List<AdditionalDto> additionalDtos);

  List<AdditionalDto> fromListEntity(List<Additional> additionals);
}
