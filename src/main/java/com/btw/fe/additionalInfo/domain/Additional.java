package com.btw.fe.additionalInfo.domain;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "ca_encdocvta_adicional_fe")
@AllArgsConstructor
@NoArgsConstructor
@IdClass(AdditionalPK.class)
public class Additional {

  @Id
  @GeneratedValue(generator="sequenciaDePrueba")
  @SequenceGenerator(name="sequenciaDePrueba",sequenceName="seq_additional", allocationSize=1)
  private Long edvsecuencia;
  @Id
  private String numerofactura;
  private String edvtipoconsclie;
  private String edvnumedocuclie;
  private String codigoprestador;
  private String tipodocumento;
  private String numeroidentificacion;
  private String primerapellido;
  private String segundoapellido;
  private String primernombre;
  private String segundonombre;
  private String tipousuario;
  private String modalidadcontrato;
  @Column(name = "coberturaoplandebeneficios")
  private String cobertura;
  private String numeroautorizacion;
  private String numeroprescripcion;
  private String numeroentregaprescripcion;
  private String numerocontrato;
  private String numeropoliza;
  private BigDecimal copago;
  private BigDecimal cuotamoderado;
  private BigDecimal cuotarecuperacion;
  private String pagoscompartidos;
  private LocalDate fechainicio;
  private LocalDate fechafinal;

  public Long getEdvsecuencia() {
    return edvsecuencia;
  }

  public void setEdvsecuencia(Long edvsecuencia) {
    this.edvsecuencia = edvsecuencia;
  }

  public String getNumerofactura() {
    return numerofactura;
  }

  public void setNumerofactura(String numerofactura) {
    this.numerofactura = numerofactura;
  }

  public String getEdvtipoconsclie() {
    return edvtipoconsclie;
  }

  public void setEdvtipoconsclie(String edvtipoconsclie) {
    this.edvtipoconsclie = edvtipoconsclie;
  }

  public String getEdvnumedocuclie() {
    return edvnumedocuclie;
  }

  public void setEdvnumedocuclie(String edvnumedocuclie) {
    this.edvnumedocuclie = edvnumedocuclie;
  }

  public String getCodigoprestador() {
    return codigoprestador;
  }

  public void setCodigoprestador(String codigoprestador) {
    this.codigoprestador = codigoprestador;
  }

  public String getTipodocumento() {
    return tipodocumento;
  }

  public void setTipodocumento(String tipodocumento) {
    this.tipodocumento = tipodocumento;
  }

  public String getNumeroidentificacion() {
    return numeroidentificacion;
  }

  public void setNumeroidentificacion(String numeroidentificacion) {
    this.numeroidentificacion = numeroidentificacion;
  }

  public String getPrimerapellido() {
    return primerapellido;
  }

  public void setPrimerapellido(String primerapellido) {
    this.primerapellido = primerapellido;
  }

  public String getSegundoapellido() {
    return segundoapellido;
  }

  public void setSegundoapellido(String segundoapellido) {
    this.segundoapellido = segundoapellido;
  }

  public String getPrimernombre() {
    return primernombre;
  }

  public void setPrimernombre(String primernombre) {
    this.primernombre = primernombre;
  }

  public String getSegundonombre() {
    return segundonombre;
  }

  public void setSegundonombre(String segundonombre) {
    this.segundonombre = segundonombre;
  }

  public String getTipousuario() {
    return tipousuario;
  }

  public void setTipousuario(String tipousuario) {
    this.tipousuario = tipousuario;
  }

  public String getModalidadcontrato() {
    return modalidadcontrato;
  }

  public void setModalidadcontrato(String modalidadcontrato) {
    this.modalidadcontrato = modalidadcontrato;
  }

  public String getCobertura() {
    return cobertura;
  }

  public void setCobertura(String cobertura) {
    this.cobertura = cobertura;
  }

  public String getNumeroautorizacion() {
    return numeroautorizacion;
  }

  public void setNumeroautorizacion(String numeroautorizacion) {
    this.numeroautorizacion = numeroautorizacion;
  }

  public String getNumeroprescripcion() {
    return numeroprescripcion;
  }

  public void setNumeroprescripcion(String numeroprescripcion) {
    this.numeroprescripcion = numeroprescripcion;
  }

  public String getNumeroentregaprescripcion() {
    return numeroentregaprescripcion;
  }

  public void setNumeroentregaprescripcion(String numeroentregaprescripcion) {
    this.numeroentregaprescripcion = numeroentregaprescripcion;
  }

  public String getNumerocontrato() {
    return numerocontrato;
  }

  public void setNumerocontrato(String numerocontrato) {
    this.numerocontrato = numerocontrato;
  }

  public String getNumeropoliza() {
    return numeropoliza;
  }

  public void setNumeropoliza(String numeropoliza) {
    this.numeropoliza = numeropoliza;
  }

  public BigDecimal getCopago() {
    return copago;
  }

  public void setCopago(BigDecimal copago) {
    this.copago = copago;
  }

  public BigDecimal getCuotamoderado() {
    return cuotamoderado;
  }

  public void setCuotamoderado(BigDecimal cuotamoderado) {
    this.cuotamoderado = cuotamoderado;
  }

  public BigDecimal getCuotarecuperacion() {
    return cuotarecuperacion;
  }

  public void setCuotarecuperacion(BigDecimal cuotarecuperacion) {
    this.cuotarecuperacion = cuotarecuperacion;
  }

  public String getPagoscompartidos() {
    return pagoscompartidos;
  }

  public void setPagoscompartidos(String pagoscompartidos) {
    this.pagoscompartidos = pagoscompartidos;
  }

  public LocalDate getFechainicio() {
    return fechainicio;
  }

  public void setFechainicio(LocalDate fechainicio) {
    this.fechainicio = fechainicio;
  }

  public LocalDate getFechafinal() {
    return fechafinal;
  }

  public void setFechafinal(LocalDate fechafinal) {
    this.fechafinal = fechafinal;
  }
}
