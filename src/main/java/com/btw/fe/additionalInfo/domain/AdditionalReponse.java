package com.btw.fe.additionalInfo.domain;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
public class AdditionalReponse {

  private List<AdditionalDto> additionalDtos;
  private MessageResponse message;
  private String customer;

  public List<AdditionalDto> getAdditionalDtos() {
    return additionalDtos;
  }

  public void setAdditionalDtos(
      List<AdditionalDto> additionalDtos) {
    this.additionalDtos = additionalDtos;
  }

  public MessageResponse getMessage() {
    return message;
  }

  public void setMessage(MessageResponse message) {
    this.message = message;
  }

  public String getCustomer() {
    return customer;
  }

  public void setCustomer(String customer) {
    this.customer = customer;
  }
}
