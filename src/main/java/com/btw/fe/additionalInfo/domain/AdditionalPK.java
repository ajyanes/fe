package com.btw.fe.additionalInfo.domain;


import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
public class AdditionalPK implements Serializable {

  private Long edvsecuencia;
  private String numerofactura;
}
