package com.btw.fe.additionalInfo.domain;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
public class AdditionalUpdateDto {

  private String numerofactura;
  private String codigoprestador;
  private String modalidadcontrato;
  private String cobertura;
  private String numerocontrato;
  private String numeropoliza;
  private BigDecimal copago;
  private BigDecimal cuotamoderado;
  private BigDecimal cuotarecuperacion;
  private String pagoscompartidos;
  private LocalDate fechainicio;
  private LocalDate fechafinal;

  public String getNumerofactura() {
    return numerofactura;
  }

  public void setNumerofactura(String numerofactura) {
    this.numerofactura = numerofactura;
  }

  public String getCodigoprestador() {
    return codigoprestador;
  }

  public String getModalidadcontrato() {
    return modalidadcontrato;
  }

  public void setModalidadcontrato(String modalidadcontrato) {
    this.modalidadcontrato = modalidadcontrato;
  }

  public void setCodigoprestador(String codigoprestador) {
    this.codigoprestador = codigoprestador;
  }

  public String getCobertura() {
    return cobertura;
  }

  public void setCobertura(String cobertura) {
    this.cobertura = cobertura;
  }

  public String getNumerocontrato() {
    return numerocontrato;
  }

  public void setNumerocontrato(String numerocontrato) {
    this.numerocontrato = numerocontrato;
  }

  public String getNumeropoliza() {
    return numeropoliza;
  }

  public void setNumeropoliza(String numeropoliza) {
    this.numeropoliza = numeropoliza;
  }

  public BigDecimal getCopago() {
    return copago;
  }

  public void setCopago(BigDecimal copago) {
    this.copago = copago;
  }

  public BigDecimal getCuotamoderado() {
    return cuotamoderado;
  }

  public void setCuotamoderado(BigDecimal cuotamoderado) {
    this.cuotamoderado = cuotamoderado;
  }

  public BigDecimal getCuotarecuperacion() {
    return cuotarecuperacion;
  }

  public void setCuotarecuperacion(BigDecimal cuotarecuperacion) {
    this.cuotarecuperacion = cuotarecuperacion;
  }

  public String getPagoscompartidos() {
    return pagoscompartidos;
  }

  public void setPagoscompartidos(String pagoscompartidos) {
    this.pagoscompartidos = pagoscompartidos;
  }

  public LocalDate getFechainicio() {
    return fechainicio;
  }

  public void setFechainicio(LocalDate fechainicio) {
    this.fechainicio = fechainicio;
  }

  public LocalDate getFechafinal() {
    return fechafinal;
  }

  public void setFechafinal(LocalDate fechafinal) {
    this.fechafinal = fechafinal;
  }
}
