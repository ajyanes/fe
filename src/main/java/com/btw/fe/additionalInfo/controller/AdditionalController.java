package com.btw.fe.additionalInfo.controller;

import com.btw.fe.additionalInfo.domain.AdditionalDto;
import com.btw.fe.additionalInfo.domain.AdditionalReponse;
import com.btw.fe.additionalInfo.domain.AdditionalUpdateDto;
import com.btw.fe.additionalInfo.domain.MessageResponse;
import com.btw.fe.additionalInfo.service.AdditionalService;
import java.util.List;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/additional")
@CrossOrigin(origins = "*")
public class AdditionalController {

  @Autowired
  private AdditionalService additionalService;

  @PostMapping(value = "", consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> save(@RequestBody AdditionalReponse additionalReponse) {
    AdditionalReponse response = additionalService.saveInfo(additionalReponse);

    if (response != null) {
      response.setMessage(new MessageResponse("Registro Exitoso!!"));
      return new ResponseEntity<>(response, HttpStatus.OK);
    } else {
      return new ResponseEntity<>(new MessageResponse("No se pudo guardar el registro"),
          HttpStatus.BAD_REQUEST);
    }
  }

  @GetMapping(value = "/find/{tipo}/{cons}", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> get(@PathVariable("tipo") String tipocons, @PathVariable("cons") String cons) {
    AdditionalReponse additionalReponse = additionalService.get(tipocons, cons);

    if (!Objects.isNull(additionalReponse)) {
      return new ResponseEntity<>(additionalReponse, HttpStatus.OK);
    } else {
      additionalReponse.setMessage(new MessageResponse("No se encontró el registro " + tipocons+cons));
      return new ResponseEntity<>(additionalReponse, HttpStatus.BAD_REQUEST);
    }
  }

  @DeleteMapping(value = "/{id}/{factura}")
  public ResponseEntity<?> delete(@PathVariable("id") String id, @PathVariable("factura") String factura) {
    AdditionalDto additionalDto = new AdditionalDto();

    additionalService.delete(id, factura);

    additionalDto.setMessage(new MessageResponse("Se ha borrado el registro " + factura));

    return new ResponseEntity<>(additionalDto, HttpStatus.OK);
  }

  @PatchMapping(value = "", consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<?> update(@RequestBody AdditionalReponse additionalReponse){
    AdditionalReponse response = additionalService.update(additionalReponse);

    if (response != null) {
      response.setMessage(new MessageResponse("Registro Actualizado!!"));
      return new ResponseEntity<>(response, HttpStatus.OK);
    } else {
      return new ResponseEntity<>(new MessageResponse("No se pudo actualizar el registro"),
          HttpStatus.BAD_REQUEST);
    }
  }


}
