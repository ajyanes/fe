package com.btw.fe.Util;

public enum MessageResource {

  RECORD_NOT_EXISTS("El registro consultado no existe"),
  INITIAL_DATE_AFTER_FINAL_DATE("La fecha inicial no debe ser mayor a la fecha final"),
  RECORD_EXISTS("Existe un registro con el mismo número de factura");

  private String name;

  MessageResource(String name) {
    this.name = name;
  }

  public String getName(){
    return name;
  }

}
